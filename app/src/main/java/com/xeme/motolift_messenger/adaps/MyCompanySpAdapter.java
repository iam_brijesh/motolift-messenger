package com.xeme.motolift_messenger.adaps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xeme.motolift_messenger.pojos.City;

import java.util.List;

/**
 * Created by root on 30/8/16.
 */
public class MyCompanySpAdapter extends BaseAdapter {


    List<City> MCitiesLists;
    Context ctx;
    LayoutInflater layoutInflater;

    public MyCompanySpAdapter(List<City> MCitiesLists, Context ctx) {
        this.MCitiesLists = MCitiesLists;
        this.layoutInflater = LayoutInflater.from(ctx);
        this.ctx = ctx;
    }


    @Override
    public int getCount() {
        return MCitiesLists.size();
    }

    @Override
    public Object getItem(int position) {
        return MCitiesLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        City mObj = MCitiesLists.get(position);
        View vi = convertView;
        if (convertView == null)
            vi = layoutInflater.inflate(android.R.layout.simple_spinner_item, null);

        TextView title = (TextView) vi.findViewById(android.R.id.text1);
        String Fullname = mObj.name;
        title.setText(Fullname);
        return vi;


    }
}
