package com.xeme.motolift_messenger.adaps;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;

import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Availableorder;
import com.xeme.motolift_messenger.utils.MyHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAvailebleOrdersRVAdapter extends RecyclerView.Adapter<MyAvailebleOrdersRVAdapter.ViewHolder> {

    private final List<Availableorder> mValues;

    int pos;
    Context mContext;
    private MyAORvHelper myHelper;

    public MyAvailebleOrdersRVAdapter(List<Availableorder> items, MyAORvHelper myHelper, Context ctx) {
        mValues = items;
        this.myHelper = myHelper;
        mContext = ctx;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_home_listitem, parent, false);
        return new ViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.mItem = mValues.get(position);
        pos = position;

        holder.fhlActvOrderdatetime.setText(holder.mItem.dateTime);

        holder.fhlActvAddressBodyA.setText(holder.mItem.start);
        holder.fhlActvAddressBodyB.setText(holder.mItem.destination);


        holder.fhlActvContactNameA.setText(holder.mItem.contactpersonA);
        holder.fhlActvContactNameB.setText(holder.mItem.contactpesonB);


        holder.fhlActvContactPhoneA.setText(holder.mItem.mobileA);
        holder.fhlActvContactPhoneB.setText(holder.mItem.mobileB);

        holder.fhlActvPrice.setText(holder.mItem.price);
        holder.fhlActvDistance.setText(holder.mItem.distance);


        holder.mView.setOnClickListener(v -> {
            if (null != myHelper) {

                if (MyHelper.isAndroid5()) {

                    Animator animator = ViewAnimationUtils.createCircularReveal(
                            holder.mView,
                            holder.mView.getWidth() / 2,
                            holder.mView.getHeight() / 2,
                            0,
                            (float) Math.hypot(holder.mView.getWidth(), holder.mView.getHeight()));
                    animator.setDuration(500);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.start();
                    animator.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                            myHelper.OnOrderSelected(mValues.get(position));
                            // getDrawQR(finalQrDetails_header + finalQrDetails_body);
                            // myHelper.OnEventSelected(mValues.get(position).id);
                           // Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.origin.lat+" Long:"+mValues.get(position).latlong.origin._long, Toast.LENGTH_SHORT).show();
                            //Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.destination.lat+" Long:"+mValues.get(position).latlong.destination._long, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                } else {
                    //getDrawQR(finalQrDetails_header + finalQrDetails_body);
                     myHelper.OnOrderSelected(mValues.get(position));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



    public interface MyAORvHelper {

        public void OnOrderSelected(Availableorder mAvailableorder);
        //public void OnEventSelected(String EventId);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        public final View mView;
        public Availableorder mItem;

        @BindView(R.id.fhl_actv_orderdatetime)
        AppCompatTextView fhlActvOrderdatetime;

        @BindView(R.id.la_apiv_icon)
        AppCompatImageView laApivIcon;

        @BindView(R.id.fhl_actv_address_body_a)
        AppCompatTextView fhlActvAddressBodyA;

        @BindView(R.id.fhl_actv_contact_name_a)
        AppCompatTextView fhlActvContactNameA;

        @BindView(R.id.fhl_actv_contact_phone_a)
        AppCompatTextView fhlActvContactPhoneA;

        @BindView(R.id.fhl_actv_address_body_b)
        AppCompatTextView fhlActvAddressBodyB;

        @BindView(R.id.fhl_actv_contact_name_b)
        AppCompatTextView fhlActvContactNameB;

        @BindView(R.id.fhl_actv_contact_phone_b)
        AppCompatTextView fhlActvContactPhoneB;

        @BindView(R.id.fhl_actv_price)
        AppCompatTextView fhlActvPrice;

        @BindView(R.id.fhl_actv_distance)
        AppCompatTextView fhlActvDistance;

        @BindView(R.id.fhl_ll_main)
        LinearLayout mllItemName;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            fhlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fhlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
        }


        @Override
        public String toString() {
            return "";
        }
    }
}
