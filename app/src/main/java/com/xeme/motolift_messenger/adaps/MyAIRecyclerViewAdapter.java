package com.xeme.motolift_messenger.adaps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Contact_Pojo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAIRecyclerViewAdapter extends RecyclerView.Adapter<MyAIRecyclerViewAdapter.ViewHolder> {


    private final List<Contact_Pojo> mValues;
    int pos;
    Context mContext;
    private MyRvHelper myHelper;


    public MyAIRecyclerViewAdapter(List<Contact_Pojo> items, MyRvHelper myHelper, Context ctx) {
        mValues = items;
        this.myHelper = myHelper;
        mContext = ctx;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_invite_listitem, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.mItem = mValues.get(position);
        pos = position;
        holder.name.setText(holder.mItem.getName());
        holder.email.setText(holder.mItem.getEmail());
        holder.cb_is_checked.setOnCheckedChangeListener(null);
        if (holder.mItem.isSelected()) {
            holder.cb_is_checked.setChecked(true);
        } else {
            holder.cb_is_checked.setChecked(false);
        }

        holder.cb_is_checked.setOnCheckedChangeListener((buttonView, isChecked) -> {
             mValues.get(position).setSelected(isChecked);
            if (myHelper != null) {
                myHelper.OnContactSelected(holder.mItem, position, isChecked);
            }

        });


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public interface MyRvHelper {
        public void OnEventSelected(String EventId);

        public void OnContactSelected(Contact_Pojo mContact_pojo, int Pos, boolean isSelected);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        public final View mView;
        public Contact_Pojo mItem;

        @BindView(R.id.aili_actv_name)
        AppCompatTextView name;

        @BindView(R.id.aili_actv_email)
        AppCompatTextView email;

        @BindView(R.id.aili_accb_choose)
        AppCompatCheckBox cb_is_checked;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

        }


        @Override
        public String toString() {
            return "";
        }
    }
}
