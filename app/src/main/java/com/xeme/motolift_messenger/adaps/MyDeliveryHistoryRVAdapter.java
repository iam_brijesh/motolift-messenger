package com.xeme.motolift_messenger.adaps;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Messengerorder;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyDeliveryHistoryRVAdapter extends RecyclerView.Adapter<MyDeliveryHistoryRVAdapter.ViewHolder> {

    private final List<Messengerorder> mValues;

    public  final String ORDER_STATUS_0="In Progress";
    public  final String ORDER_STATUS_1="Completed";
    public  final String ORDER_STATUS_2="On Hold";
    public  final String ORDER_STATUS_3="Cancel";

    int pos;
    Context mContext;
    private MyDHRvHelper myHelper;

    public MyDeliveryHistoryRVAdapter(List<Messengerorder> items, MyDHRvHelper myHelper, Context ctx) {
        mValues = items;
        this.myHelper = myHelper;
        mContext = ctx;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_my_deliveries_listitem, parent, false);
        return new ViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        pos = position;

        holder.fmdlActvOrderdatetime.setText(holder.mItem.dateTime);

        holder.fmdlActvAddressBodyA.setText(holder.mItem.start);
        holder.fmdlActvAddressBodyB.setText(holder.mItem.destination);


        holder.fmdlActvContactNameA.setText(holder.mItem.contactpersonA);
        holder.fmdlActvContactNameB.setText(holder.mItem.contactpesonB);


        holder.fmdlActvContactPhoneA.setText(holder.mItem.mobileA);
        holder.fmdlActvContactPhoneB.setText(holder.mItem.mobileB);

        holder.fmdlActvPrice.setText(holder.mItem.price);


        if (holder.mItem.status.equals("0")){

            holder.fmdlActvStatus.setText(MyConsta.ORDER_STATUS_0);
            holder.fmdlActvStatus.setTextColor(Color.BLUE);

        }
        else if (holder.mItem.status.equals("1")){
            holder.fmdlActvStatus.setText(MyConsta.ORDER_STATUS_1);
            holder.fmdlActvStatus.setTextColor(Color.BLUE);


        }
        else if (holder.mItem.status.equals("2")){
            holder.fmdlActvStatus.setText(MyConsta.ORDER_STATUS_2);
            holder.fmdlActvStatus.setTextColor(Color.YELLOW);
        }
        else if (holder.mItem.status.equals("3")){
            holder.fmdlActvStatus.setText(MyConsta.ORDER_STATUS_3);
            holder.fmdlActvStatus.setTextColor(Color.GREEN);
            holder.fmdlAcbtnRate.setEnabled(true);
            holder.fmdlAcbtnRate.setAlpha(1);

        }


        if (Integer.valueOf( holder.mItem.ratebymessanger)>0){

            holder.fmdlAcbtnRate.setText(Integer.valueOf( holder.mItem.ratebymessanger)>1?Integer.valueOf( holder.mItem.ratebymessanger)+" Stars":Integer.valueOf( holder.mItem.ratebymessanger)+" Star");

        }



            holder.fmdlAcbtnRate.setOnClickListener(v -> {
                if (null != myHelper) {
                    myHelper.OnClickRate(holder.mItem.id,Integer.valueOf( holder.mItem.ratebymessanger));
                }
            });



        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != myHelper) {

                    if (MyHelper.isAndroid5()) {

                        Animator animator = ViewAnimationUtils.createCircularReveal(
                                holder.mView,
                                holder.mView.getWidth() / 2,
                                holder.mView.getHeight() / 2,
                                0,
                                (float) Math.hypot(holder.mView.getWidth(), holder.mView.getHeight()));
                        animator.setDuration(500);
                        animator.setInterpolator(new AccelerateDecelerateInterpolator());
                        animator.start();
                        animator.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                // getDrawQR(finalQrDetails_header + finalQrDetails_body);
                                // myHelper.OnEventSelected(mValues.get(position).id);
                                Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.origin.lat+" Long:"+mValues.get(position).latlong.origin._long, Toast.LENGTH_SHORT).show();
                                Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.destination.lat+" Long:"+mValues.get(position).latlong.destination._long, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    } else {
                        //getDrawQR(finalQrDetails_header + finalQrDetails_body);
                        // myHelper.OnEventSelected(mValues.get(position).id);
                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



    public interface MyDHRvHelper {
        public void OnEventSelected(String EventId);
        public void OnClickRate(String id,int Rated);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        public final View mView;
        public Messengerorder mItem;

        @BindView(R.id.fmdl_actv_orderdatetime)
        AppCompatTextView fmdlActvOrderdatetime;

        @BindView(R.id.la_apiv_icon)
        AppCompatImageView laApivIcon;

        @BindView(R.id.fmdl_actv_address_body_a)
        AppCompatTextView fmdlActvAddressBodyA;

        @BindView(R.id.fmdl_actv_contact_name_a)
        AppCompatTextView fmdlActvContactNameA;

        @BindView(R.id.fmdl_actv_contact_phone_a)
        AppCompatTextView fmdlActvContactPhoneA;

        @BindView(R.id.fmdl_actv_address_body_b)
        AppCompatTextView fmdlActvAddressBodyB;

        @BindView(R.id.fmdl_actv_contact_name_b)
        AppCompatTextView fmdlActvContactNameB;

        @BindView(R.id.fmdl_actv_contact_phone_b)
        AppCompatTextView fmdlActvContactPhoneB;

        @BindView(R.id.fmdl_actv_price)
        AppCompatTextView fmdlActvPrice;

        @BindView(R.id.fmdl_actv_status)
        AppCompatTextView fmdlActvStatus;

        @BindView(R.id.fmdl_acbtn_rate)
        AppCompatButton fmdlAcbtnRate;

        @BindView(R.id.fmdl_ll_main)
        LinearLayout mllItemName;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            fmdlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fmdlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fmdlAcbtnRate.setEnabled(false);
            fmdlAcbtnRate.setAlpha(0.4F);
        }


        @Override
        public String toString() {
            return "";
        }
    }
}
