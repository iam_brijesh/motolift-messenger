package com.xeme.motolift_messenger.adaps;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Messengerorder;
import com.xeme.motolift_messenger.utils.MyConsta;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyCheckInOutRVAdapter extends RecyclerView.Adapter<MyCheckInOutRVAdapter.ViewHolder> {

    private static final String KEY_CHECKED = "1";
    private static final String KEY_UNCHECKED = "0";
    public final String ORDER_STATUS_0 = "In Progress";
    public final String ORDER_STATUS_1 = "Completed";
    public final String ORDER_STATUS_2 = "On Hold";
    public final String ORDER_STATUS_3 = "Cancel";
    private final List<Messengerorder> mValues;
    int pos;
    Context mContext;
    float KEY_ENABLE = 1.0F;
    float KEY_DISABLE = 0.6F;
    private MyCIORvHelper myHelper;

    public MyCheckInOutRVAdapter(List<Messengerorder> items, MyCIORvHelper myHelper, Context ctx) {
        mValues = items;
        this.myHelper = myHelper;
        mContext = ctx;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_check_in_out_listitem, parent, false);
        return new ViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.mItem = mValues.get(position);
        pos = position;

        holder.fcioActvOrderdatetime.setText(holder.mItem.dateTime);

        holder.fcioActvAddressBodyA.setText(holder.mItem.start);
        holder.fcioActvAddressBodyB.setText(holder.mItem.destination);


        holder.fcioActvContactNameA.setText(holder.mItem.contactpersonA);
        holder.fcioActvContactNameB.setText(holder.mItem.contactpesonB);


        if (holder.mItem.ischeckin.equals(KEY_UNCHECKED) && holder.mItem.ischeckout.equals(KEY_UNCHECKED)) {
            holder.fcioAcbtnCheckin.setEnabled(true);
            holder.fcioAcbtnCheckin.setAlpha(KEY_ENABLE);
            holder.fcioAcbtnCheckout.setEnabled(false);
            holder.fcioAcbtnCheckout.setAlpha(KEY_DISABLE);

        } else if (holder.mItem.ischeckin.equals(KEY_CHECKED) && holder.mItem.ischeckout.equals(KEY_UNCHECKED)) {
            holder.fcioAcbtnCheckout.setEnabled(true);
            holder.fcioAcbtnCheckout.setAlpha(KEY_ENABLE);
            holder.fcioAcbtnCheckin.setEnabled(false);
            holder.fcioAcbtnCheckin.setAlpha(KEY_DISABLE);
        } else if (holder.mItem.ischeckin.equals(KEY_CHECKED) && holder.mItem.ischeckout.equals(KEY_CHECKED)) {
            holder.fcioAcbtnCheckin.setEnabled(false);
            holder.fcioAcbtnCheckin.setAlpha(KEY_DISABLE);
            holder.fcioAcbtnCheckout.setEnabled(false);
            holder.fcioAcbtnCheckout.setAlpha(KEY_DISABLE);
        }


        holder.fcioAcbtnCheckin.setOnClickListener(v -> {
            if (myHelper != null) {
                myHelper.OnCheckIN(holder.mItem);
            }

        });
        holder.fcioAcbtnCheckout.setOnClickListener(v -> {
            if (myHelper != null) {
                myHelper.OnCheckOUT(holder.mItem);
            }
        });

        if (holder.mItem.status.equals("0")) {
            holder.fcioActvStatus.setText(MyConsta.ORDER_STATUS_0);
            holder.fcioActvStatus.setTextColor(Color.BLUE);

        } else if (holder.mItem.status.equals("1")) {
            holder.fcioActvStatus.setText(MyConsta.ORDER_STATUS_1);
            holder.fcioActvStatus.setTextColor(Color.BLUE);
        } else if (holder.mItem.status.equals("2")) {
            holder.fcioActvStatus.setText(MyConsta.ORDER_STATUS_2);
            holder.fcioActvStatus.setTextColor(Color.YELLOW);
        } else if (holder.mItem.status.equals("3")) {
            holder.fcioActvStatus.setText(MyConsta.ORDER_STATUS_3);
            holder.fcioActvStatus.setTextColor(Color.GREEN);

        }


       /* holder.mView.setOnClickListener(v -> {
            if (null != myHelper) {

                if (MyHelper.isAndroid5()) {

                    Animator animator = ViewAnimationUtils.createCircularReveal(
                            holder.mView,
                            holder.mView.getWidth() / 2,
                            holder.mView.getHeight() / 2,
                            0,
                            (float) Math.hypot(holder.mView.getWidth(), holder.mView.getHeight()));
                    animator.setDuration(500);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.start();
                    animator.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                            myHelper.OnOrderSelected(mValues.get(position));
                            // getDrawQR(finalQrDetails_header + finalQrDetails_body);
                            // myHelper.OnEventSelected(mValues.get(position).id);
                            // Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.origin.lat+" Long:"+mValues.get(position).latlong.origin._long, Toast.LENGTH_SHORT).show();
                            //Toast.makeText(mContext, "Lat:"+mValues.get(position).latlong.destination.lat+" Long:"+mValues.get(position).latlong.destination._long, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                } else {
                    //getDrawQR(finalQrDetails_header + finalQrDetails_body);
                    myHelper.OnOrderSelected(mValues.get(position));
                }
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public interface MyCIORvHelper {

        //public void OnOrderSelected(Availableorder mAvailableorder);
        //public void OnEventSelected(String EventId);
        void OnCheckIN(Messengerorder mMessengerorder);

        void OnCheckOUT(Messengerorder mMessengerorder);
    }


/*    static class ViewHolder extends RecyclerView.ViewHolder {


        public final View mView;
        public Availableorder mItem;

        @BindView(R.id.fhl_actv_orderdatetime)
        AppCompatTextView fhlActvOrderdatetime;

        @BindView(R.id.la_apiv_icon)
        AppCompatImageView laApivIcon;

        @BindView(R.id.fhl_actv_address_body_a)
        AppCompatTextView fhlActvAddressBodyA;

        @BindView(R.id.fhl_actv_contact_name_a)
        AppCompatTextView fhlActvContactNameA;

        @BindView(R.id.fhl_actv_contact_phone_a)
        AppCompatTextView fhlActvContactPhoneA;

        @BindView(R.id.fhl_actv_address_body_b)
        AppCompatTextView fhlActvAddressBodyB;

        @BindView(R.id.fhl_actv_contact_name_b)
        AppCompatTextView fhlActvContactNameB;

        @BindView(R.id.fhl_actv_contact_phone_b)
        AppCompatTextView fhlActvContactPhoneB;

        @BindView(R.id.fhl_actv_price)
        AppCompatTextView fhlActvPrice;

        @BindView(R.id.fhl_actv_distance)
        AppCompatTextView fhlActvDistance;

        @BindView(R.id.fhl_ll_main)
        LinearLayout mllItemName;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            fhlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fhlActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
        }


        @Override
        public String toString() {
            return "";
        }
    }*/

    static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public Messengerorder mItem;


        @BindView(R.id.fcio_actv_orderdatetime)
        AppCompatTextView fcioActvOrderdatetime;
        @BindView(R.id.fcio_actv_address_body_a)
        AppCompatTextView fcioActvAddressBodyA;
        @BindView(R.id.fcio_actv_contact_name_a)
        AppCompatTextView fcioActvContactNameA;
        @BindView(R.id.fcio_actv_address_body_b)
        AppCompatTextView fcioActvAddressBodyB;
        @BindView(R.id.fcio_actv_contact_name_b)
        AppCompatTextView fcioActvContactNameB;
        @BindView(R.id.fcio_actv_status)
        AppCompatTextView fcioActvStatus;
        @BindView(R.id.fcio_acbtn_checkin)
        AppCompatButton fcioAcbtnCheckin;
        @BindView(R.id.fcio_acbtn_checkout)
        AppCompatButton fcioAcbtnCheckout;
        @BindView(R.id.fcio_ll_main)
        LinearLayout fcioLlMain;

        ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);

            fcioActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fcioActvAddressBodyA.setMovementMethod(new ScrollingMovementMethod());
            fcioAcbtnCheckin.setEnabled(false);
            fcioAcbtnCheckin.setAlpha(0.4F);
            fcioAcbtnCheckout.setEnabled(false);
            fcioAcbtnCheckout.setAlpha(0.4F);
        }
    }
}
