package com.xeme.motolift_messenger.CustomViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;

/**
 * Created by root on 23/8/16.
 */
public class ScrollableTextView extends AppCompatTextView {

    private ViewParent mViewParent;
    public ScrollableTextView(Context context) {
        super(context);
    }

    public ScrollableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setViewParent(@Nullable final ViewParent viewParent) { //any ViewGroup
        mViewParent = viewParent;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        boolean ret;
        ret = super.dispatchTouchEvent(event);
        if(ret)
        {
            mViewParent.requestDisallowInterceptTouchEvent(true);
        }
        return ret;
    }

}
