package com.xeme.motolift_messenger.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 24/8/16.
 */
public class Invite_Request_pojo {

    String mid;
    String apikey;
    List<Contact_Pojo> mSelectedInvities=new ArrayList<>();

    public Invite_Request_pojo(String cid, String UUID, List<Contact_Pojo> selectedInvities) {
        this.mid = cid;
        this.apikey = UUID;
        mSelectedInvities = selectedInvities;
    }
}
