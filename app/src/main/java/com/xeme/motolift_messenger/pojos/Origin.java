
package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Origin {

    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("long")
    @Expose
    public String _long;

}
