package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class City {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("zipcode")
    @Expose
    public String zipcode;

}