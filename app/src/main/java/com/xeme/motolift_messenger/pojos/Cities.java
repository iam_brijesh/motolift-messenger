package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Cities {
    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("Cities")
    @Expose
    public List<City> citiesList = new ArrayList<City>();

}