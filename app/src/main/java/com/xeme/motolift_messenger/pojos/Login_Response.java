package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 13/8/16.
 */
public class Login_Response {
    @SerializedName("IsBikeAssigned")
    @Expose
    public Boolean isBikeAssigned;
    @SerializedName("BikeID")
    @Expose
    public Integer bikeID;
    @SerializedName("BikeName")
    @Expose
    public String bikeName;
    @SerializedName("BikeBrand")
    @Expose
    public String bikeBrand;
    @SerializedName("BikeModel")
    @Expose
    public String bikeModel;
    @SerializedName("BikePatent")
    @Expose
    public String bikePatent;
    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("mid")
    @Expose
    public Integer mid;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("profileimage")
    @Expose
    public String profileimage;
    @SerializedName("regifrom")
    @Expose
    public Integer regifrom;
    @SerializedName("apikey")
    @Expose
    public String apikey;
    @SerializedName("is_active")
    @Expose
    public Integer is_active;
    @SerializedName("CityName")
    @Expose
    public String cityName;
    @SerializedName("CityZipCode")
    @Expose
    public Integer cityZipCode;
    @SerializedName("IsCompanyAssined")
    @Expose
    public Boolean isCompanyAssined;
    @SerializedName("CompanyFisrtName")
    @Expose
    public String companyFisrtName;
    @SerializedName("CompanyLastName")
    @Expose
    public String companyLastName;
    @SerializedName("CompanyID")
    @Expose
    public Integer companyID;
}






