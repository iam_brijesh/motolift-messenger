
package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Messengerorder {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("start")
    @Expose
    public String start;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("contactpersonA")
    @Expose
    public String contactpersonA;
    @SerializedName("contactpesonB")
    @Expose
    public String contactpesonB;
    @SerializedName("latlong")
    @Expose
    public Latlong latlong;
    @SerializedName("mobileA")
    @Expose
    public String mobileA;
    @SerializedName("mobileB")
    @Expose
    public String mobileB;
    @SerializedName("distance")
    @Expose
    public String distance;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("apikey")
    @Expose
    public String apikey;
    @SerializedName("chargeid")
    @Expose
    public String chargeid;
    @SerializedName("payment")
    @Expose
    public String payment;
    @SerializedName("voucher")
    @Expose
    public String voucher;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("u_id")
    @Expose
    public String uId;
    @SerializedName("date_time")
    @Expose
    public String dateTime;
    @SerializedName("ratebymessanger")
    @Expose
    public String ratebymessanger;
    @SerializedName("ischeckin")
    @Expose
    public String ischeckin;
    @SerializedName("ischeckout")
    @Expose
    public String ischeckout;

}
