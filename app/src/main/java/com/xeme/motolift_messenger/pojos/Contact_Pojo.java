package com.xeme.motolift_messenger.pojos;

/**
 * Created by root on 24/8/16.
 */
public class Contact_Pojo {

    long id;
    String Name;
    String Email;
    boolean IsSelected;

    public Contact_Pojo(long id, String name, String email, boolean isSelected) {
        this.id = id;
        Name = name;
        Email = email;
        IsSelected = isSelected;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSelected() {
        return IsSelected;
    }

    public void setSelected(boolean selected) {
        IsSelected = selected;
    }
}
