
package com.xeme.motolift_messenger.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Latlong {

    @SerializedName("origin")
    @Expose
    public Origin origin;
    @SerializedName("destination")
    @Expose
    public Destination destination;

}
