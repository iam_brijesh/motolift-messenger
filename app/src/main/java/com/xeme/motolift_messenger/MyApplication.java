package com.xeme.motolift_messenger;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.AppConfig;
import com.xeme.motolift_messenger.utils.GoogleApiHelper;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/**
 * Created by roga on 18/07/16.
 */
public class MyApplication extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static boolean isconnected = false;
    public static MyApplication instance = null;
    public static Retrofit mRetrofit = null;
    public static SlimApi service = null;
    public static GoogleApiHelper googleApiHelper;
    public static MyDatahelper MyDB;
    public static Typeface Typeface_Light = null;
    public static Typeface Typeface_Thin = null;
    GoogleApiClient mGoogleApiClient;
    String UUID = "";
    int UID = 0;

    public MyApplication() {
    }

    public static String getUUID() {
        return MyDB.getString(MyConsta.KEY_UUID, "");
    }

    public static String getProfileImage() {
        // Logwtf("getProfileImage()","Image Link:"+MyDB.getString(MyConsta.KEY_ProfileImage, ""));
        return MyDB.getString(MyConsta.KEY_ProfileImage, "");

    }

    public static int getUID() {
        return MyDB.getInt(MyConsta.KEY_CID, 0);
    }

    public static String getFullname() {
        return MyDB.getString(MyConsta.KEY_FullName, "");
    }

    public static String getName() {
        return MyDB.getString(MyConsta.KEY_Name, "");
    }

    public static String getMSurname() {
        return MyDB.getString(MyConsta.KEY_Surname, "");
    }

    public static Retrofit getmRetrofit() {
        return mRetrofit;
    }

    public static SlimApi getService() {
        return service;
    }

    public static MyApplication getInstance() {
        if (null == instance) {
            instance = new MyApplication();
        }
        return instance;
    }

    public static GoogleApiHelper getGoogleApiHelper() {
        return getInstance().getGoogleApiHelperInstance();
    }

    public GoogleApiClient getGoogleApiClient() {

        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
        return mGoogleApiClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(2, TimeUnit.MINUTES);
        client.setConnectTimeout(2, TimeUnit.MINUTES);
        client.setRetryOnConnectionFailure(true);
        client.setWriteTimeout(2, TimeUnit.MINUTES);
        MyDB = MyDatahelper.getInstance(this, MyConsta.My_Pref);
        UUID = MyDB.getString(MyConsta.KEY_UUID, "");
        UID = MyDB.getInt(MyConsta.KEY_CID, 0);
        googleApiHelper = new GoogleApiHelper(getApplicationContext());
        if (mRetrofit == null) {
            // Enable caching for OkHttp
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cache = new Cache(getApplicationContext().getCacheDir(), cacheSize);
            client.setCache(cache);


            mRetrofit = new Retrofit.Builder()
                    .baseUrl(AppConfig.URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        if (service == null) {
            service = mRetrofit.create(SlimApi.class);
        }
        initTypeface();
    }


    public void initTypeface() {
        Typeface_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoLight.ttf");
        Typeface_Thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoThin.ttf");

    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public GoogleApiHelper getGoogleApiHelperInstance() {
        return this.googleApiHelper;
    }

    protected synchronized void buildGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(MyApplication.this.getBaseContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        isconnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void UpdateNotifToken(int UID, String UUID, String Token) {


        if (UUID.length() > 0 && UID > 0) {


            Call<Simple_Response> UPDATE_FBT = MyApplication.getService().Update_FBT(UUID, UID, Token);
            UPDATE_FBT.enqueue(new Callback<Simple_Response>() {
                @Override
                public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                    if (response.code() == 200) {
                        if (response != null) {

                            if (response.body().error.equals(false)) {

                                Logwtf("Response On Success", "FBT Updated");
                            } else {

                                Logwtf("Response Fail", "FBT Not Updated");
                            }
                        }

                    } else if (response.code() == 201) {

                        Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message.toString());


                    } else {

                    }


                }

                @Override
                public void onFailure(Throwable t) {
                    Logwtf("onFailure", t.getCause().toString());
                }
            });
        } else {
            Logwtf("onUpdated", "UID OR CID NULL" + UUID + " MID" + UID);
        }


    }


    public static Typeface GetFonts(int Font_Type) {


        Typeface T = null;
        switch (Font_Type) {
            case MyConsta.Font_Light:
                T = Typeface_Light;
                break;
            case MyConsta.Font_Thin:
                T = Typeface_Thin;
                break;

        }
        return T;
    }
}