package com.xeme.motolift_messenger.utils;

import java.text.SimpleDateFormat;

/**
 * Created by root on 6/8/16.
 */
public class MyConsta {


    public static final String TAG = "MotoLift-Messenger";
    public static final String KEY_REGISTEREDFROM ="RegisteredFrom" ;
    public static final String KEY_USER_ACCOUNT ="UserAccount" ;
    public static String My_Pref="MotoLift_Messenger";

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static final SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static final String KEY_UUID ="UUID" ;
    public static final String KEY_IsLoggedIn ="IsLoggedIn" ;
    public static final String KEY_KeepMeLoggedIn ="IsLoggedIn" ;
    public static final String KEY_Email ="email" ;
    public static final String KEY_Name ="name" ;
    public static final String KEY_Surname ="surname" ;
    public static final String KEY_FullName ="FullName" ;
    public static final String OrderID ="OrderID" ;
    public static final String KEY_CID ="CID" ;

    public static final String KEY_ProfileImage ="ProfileImage" ;
    public static final String KEY_City ="City";
    public static final String KEY_PostalCode ="PostalCode";
    public static final String KEY_BirthDate ="BirthDate";
    public static final String KEY_Phone ="Phone";
    public static final String KEY_Gender ="Gender";
    public static final String KEY_Address ="Address";
    public static final String KEY_Default_Value ="";
    public static final String KEY_EVENT_ID ="EventID";
    public static final String KEY_EVENT_TITLE ="EventTitle";
    public static final String KEY_EVENT_SUB_TITLE ="EventSubTitle";
    public static final String KEY_EVENT_IMAGE ="EventImage";
    public static final String KEY_VOUCHER ="Voucher" ;
    public static final String KEY_MYSEAT_RAW ="MYSEAT_RAW" ;
    public static final String KEY_MYSEAT_COL ="MYSEAT_COL" ;
    public static final String KEY_PARKINGTYPE = "PARKINGTYPE";
    public static final String KEY_Is4Friend="Is4Friend";
    public static final String KEY_FRIEND_NAME ="FRIEND_NAME";
    public static final String KEY_FRIEND_NUMBER = "FRIEND_NUMBER";
    public static final int RESPONSE_OK = 200;
    public static final int BAD_REQUEST =400 ;
    public static final String onFailureMsg ="Sorry. Something went wrong...Please try after some time..." ;

    public static final String KEY_CompanyID ="CompanyID";
    public static final String KEY_CompanyFName ="CompanyFName";
    public static final String KEY_CompanyLName ="CompanyLName";
    public static final String KEY_CompanyFullName ="CompanyFullName";

    public static final String KEY_BIKE_ISAssigned="BIKE_ISAssigned";
    public static final String KEY_BIKE_MODEL="BIKE_MODEL";
    public static final String KEY_BIKE_BRAND="BIKE_BRAND";
    public static final String KEY_BIKE_NAME="BIKE_NAME";
    public static final String KEY_BIKE_PATENT="BIKE_PATENT";
    public static final String KEY_BIKE_ID="BIKE_ID";
    private static final String ARG_PARAM_Time = "Time";
    private static final String ARG_PARAM_LatLong_A = "LatLong_A";
    private static final String ARG_PARAM_LatLong_B = "LatLong_B";
    public static String KEY_IsCompanyAssined="IsCompanyAssined";




    //-------------for this app



    public static final String KEY_USER_PROFILE="USER_PROFILE";
    public static final String KEY_LOGIN_WITH="LOGIN_WITH";


    public static final int Login_Consta_Google=0;
    public static final int Login_Consta_Facebook=1;
    public static final int Login_Consta_Linkedin=2;
    public static final int Login_Consta_Email=3;



    //For Frag_Order_Details

    private static final String ARG_PARAM_Address_A = "Address_A";
    private static final String ARG_PARAM_Address_B = "Address_B";
    private static final String ARG_PARAM_Price = "Price";


    //-----------------------



    private static final String KEY_CHECKED = "1";
    private static final String KEY_UNCHECKED = "0";
    public final static String ORDER_STATUS_0 = "In Progress";
    public final static String ORDER_STATUS_1 = "In Progress";
    public final static String ORDER_STATUS_2 = "In Delivery";
    public final static String ORDER_STATUS_3 = "Completed";
    public final   float KEY_ENABLE = 1.0F;
    public final  float KEY_DISABLE = 0.6F;


    //Notification

    public static final String NOTIF_TYPE_SIMPLE = "0";
    public static final String NOTIF_TYPE_ORDER_ACCEPTED = "1";
    public static final String NOTIF_TYPE_BIKE_REMOVED = "2";

    public static final String NOTIF_TYPE = "type";
    public static final String NOTIF_TITLE = "title";
    public static final String NOTIF_SUBTITLE = "subtitle";
    public static final String NOTIF_CLICK_ACTION = "click_action";


    ///TypeFace font

    public static final int Font_Light=1;
    public static final int Font_Thin=2;


}
