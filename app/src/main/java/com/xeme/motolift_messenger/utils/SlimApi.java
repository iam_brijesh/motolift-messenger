package com.xeme.motolift_messenger.utils;

import com.xeme.motolift_messenger.pojos.AvailableOrdersMain;
import com.xeme.motolift_messenger.pojos.Cities;
import com.xeme.motolift_messenger.pojos.City;
import com.xeme.motolift_messenger.pojos.Invite_Request_pojo;
import com.xeme.motolift_messenger.pojos.Login_Response;
import com.xeme.motolift_messenger.pojos.MyDeliveries;
import com.xeme.motolift_messenger.pojos.Simple_Response;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by root on 28/6/16.
 */
public interface SlimApi {

   /*



    @FormUrlEncoded
    @POST(AppConfig.URL_PARKING)
    Call<RegisterParkingResponse> RegisterParking(@Field("eventid") int eventid,
                                                  @Field("cardid") int cardid,
                                                  @Field("apikey") String apikey,
                                                  @Field("cid") int cid,
                                                  @Field("token") String token);

    @FormUrlEncoded
    @POST(AppConfig.URL_HISTORY_PARKING)
    Call<ParkingDetails> ParkingHistory(@Field("apikey") String apikey,
                                        @Field("cid") int cid);




    @GET(AppConfig.URL_GET_MENU)
    Call<GetIMenuItem_Response> GetMenu();

    @GET(AppConfig.URL_GET_EVETNS)
    Call<Events_Response> GetEvents();

    @POST(AppConfig.URL_ORDER)
    Call<Order_Responce> MakeOrder(@Body MakeOrder_Pojo makeOrder_pojo);


    @FormUrlEncoded
    @POST(AppConfig.URL_UPDATE_PROFILE)
    Call<UpdateProfileResponse> UpdateUserProfile(@Field("name") String name,
                                                  @Field("surname") String surname,
                                                  @Field("email") String email,
                                                  @Field("phone") String phone,
                                                  @Field("address") String address,
                                                  @Field("city") String city,
                                                  @Field("postalcode") String postalcode,
                                                  @Field("gender") int gender,
                                                  @Field("apikey") String apikey,
                                                  @Field("cid") int cid);

    @FormUrlEncoded
    @POST(AppConfig.URL_ORDER_HISTORY)
    Call<OrderHistoryResponse> OrderHistory(@Field("apikey") String apikey,
                                            @Field("cid") int cid);
*/


    @FormUrlEncoded
    @POST(AppConfig.URL_REGISTER)
    Call<Login_Response> RegisterUser(@Field("name") String name,
                                      @Field("surname") String surname,
                                      @Field("email") String email,
                                      @Field("password") String passwrod,
                                      @Field("mobile") String mobile,
                                      @Field("image") String image,
                                      @Field("is_regifrom") int is_regifrom,
                                      @Field("cityid") int cityId);


    @FormUrlEncoded
    @POST(AppConfig.URL_DELIVERY_HISTORY)
    Call<MyDeliveries> DeliveryHistory(@Field("apikey") String apikey,
                                       @Field("mid") int mid);


    @FormUrlEncoded
    @POST(AppConfig.URL_AVAILABLE_ORDERS)
    Call<AvailableOrdersMain> AvailableOrders(@Field("apikey") String apikey,
                                              @Field("mid") int mid);

    @FormUrlEncoded
    @POST(AppConfig.URL_ACCEPT_ORDER)
    Call<Simple_Response> AcceptOrder(@Field("apikey") String apikey,
                                      @Field("mid") int mid,
                                      @Field("id") String id);

    @FormUrlEncoded
    @POST(AppConfig.URL_RATE_USER)
    Call<Simple_Response> RateUSER(@Field("apikey") String apikey,
                                   @Field("mid") int cid,
                                   @Field("oid") String oid,
                                   @Field("rating") int ratings);

    @POST(AppConfig.URL_INVITE_USERS)
    Call<Simple_Response> SendInvitaions(@Body Invite_Request_pojo pojo);

    @FormUrlEncoded
    @POST(AppConfig.URL_LOGIN)
    Call<Login_Response> GetLoginDetails(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST(AppConfig.URL_UPDATE_FBT)
    Call<Simple_Response> Update_FBT(@Field("apikey") String apikey,
                                     @Field("mid") int cid, @Field("token") String token);

    @FormUrlEncoded
    @POST(AppConfig.URL_DO_CHECK__IN)
    Call<Simple_Response> DoCheckIn(@Field("apikey") String apikey,
                                       @Field("mid") int cid,
                                       @Field("oid") String orderid,
                                       @Field("bid") String bikeid);

    @FormUrlEncoded
    @POST(AppConfig.URL_DO_CHECK__OUT)
    Call<Simple_Response> DoCheckOut(@Field("apikey") String apikey,
                                    @Field("mid") int cid,
                                    @Field("oid") String orderid,
                                    @Field("bid") String bikeid);

    @GET(AppConfig.URL_COMPANIES)
    Call<City> Get_Companies();


    @GET(AppConfig.URL_CITIES)
    Call<Cities> Get_Cities();


    @FormUrlEncoded
    @POST(AppConfig.URL_UPDATE_PROFILE_IMAGE)
    Call<Simple_Response> UpdateProfileImage(@Field("apikey") String apikey,
                                             @Field("mid") int cid, @Field("img") String img);


    @FormUrlEncoded
    @POST(AppConfig.URL_FORGOT_PASSWORD)
    Call<Simple_Response> ForgotPassword(@Field("email") String email);



    @FormUrlEncoded
    @POST(AppConfig.URL_REQUEST_ACTIVATION)
    Call<Simple_Response> SendActivationRequest(@Field("apikey") String apikey,
                                        @Field("email") String email);


}
