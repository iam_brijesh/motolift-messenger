package com.xeme.motolift_messenger.utils;

public class AppConfig {

    public static final String URL_BASE = " http://todoinfotech.com";
    public static final String URL_LOGIN = "/motolift/tm/v1/messengerlogindd";
    public static final String URL_REGISTER = "/motolift/tm/v1/messengerregisterdd";
    public static final String URL_DELIVERY_HISTORY = "/motolift/tm/v1/deliveryhistory";
    public static final String URL_AVAILABLE_ORDERS = "/motolift/tm/v1/orderbeforeaccept";
    public static final String URL_UPDATE_FBT = "/motolift/tm/v1/updatefirebasetoken";
    public static final String URL_RATE_USER = "/motolift/tm/v1/messengerrateuser";
    public static final String URL_ACCEPT_ORDER="/motolift/tm/v1/acceptorder";
    public static final String URL_INVITE_USERS = "/motolift/tm/v1/invitemessenger";
    public static final String URL_COMPANIES = "/motolift/tm/v1/getcompany";
    public static final String URL_DO_CHECK__IN = "/motolift/tm/v1/checkinmessenger";
    public static final String URL_DO_CHECK__OUT = "/motolift/tm/v1/checkoutmessenger";
    public static final String URL_UPDATE_PROFILE_IMAGE = "/motolift/tm/v1/messengerimage";
    public static final String URL_CITIES = "/motolift/tm/v1/getcities";



    public static final String URL_MY_CARDS = "/demo/motolift/tm/v1/getcarddetail";
    public static final String URL_UPDATE_PROFILE = "/dingdong/v1/updateprofile";
    public static final String URL_REMOVE_CARD = "/demo/motolift/tm/v1/removecard";
    public static final String URL_CARD_REGISTER = "/demo/motolift/tm/v1/cardRegisterdd";
    public static final String URL_ORDER = "/demo/motolift/tm/v1/egisterorder";
    public static final String URL_PARKING = "/dingdong/v1/registerparking";
    public static final String URL_HISTORY_PARKING = "/dingdong/v1/getparkingdetail";
    public static final String URL_GET_MENU = "/dingdong/v1/getitemmenu";
    public static final String URL_GET_EVETNS = "/dingdong/v1/getevent";

    public static final String URL_FORGOT_PASSWORD = "/motolift/tm/v1/forgotpasswordmessanger";


    public static final String URL_REQUEST_ACTIVATION = "/motolift/tm/v1/resendemailmessanger";



}
