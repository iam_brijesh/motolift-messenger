package com.xeme.motolift_messenger.utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.xeme.motolift_messenger.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GetProfileInfo {


    Context mContext;
    MyDatahelper Myprefs;
    Animation mAnimation;
    Animation mAnimationShake;



    public GetProfileInfo(Context context) {
        mContext = context;
        Myprefs = MyDatahelper.getInstance(context, MyConsta.My_Pref);
        mAnimation= AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.fadein);
        mAnimationShake= AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.shake);
    }

    public static int genRandom() {
        Random r = new Random(System.currentTimeMillis());
        return 10000 + r.nextInt(20000);
    }

    public static void Logwtf(String From, String msg) {

        Log.wtf(MyConsta.TAG, "*******From:" + From + " Log:" + msg);
    }

    public Map<String, String> getUserDetails() {

        Map<String, String> Userinfo = new HashMap<String, String>();
        Userinfo.put(MyConsta.KEY_UUID, Myprefs.getString(MyConsta.KEY_UUID, ""));
        Userinfo.put(MyConsta.KEY_CID, String.valueOf(Myprefs.getInt(MyConsta.KEY_CID, 0)));
        Userinfo.put(MyConsta.KEY_Email, Myprefs.getString(MyConsta.KEY_Email, ""));
        Userinfo.put(MyConsta.KEY_Name, Myprefs.getString(MyConsta.KEY_Name, ""));
        Userinfo.put(MyConsta.KEY_Surname, Myprefs.getString(MyConsta.KEY_Surname, ""));
        Userinfo.put(MyConsta.KEY_FullName, Myprefs.getString(MyConsta.KEY_FullName, ""));
        Userinfo.put(MyConsta.KEY_Phone, Myprefs.getString(MyConsta.KEY_Phone, ""));
        Userinfo.put(MyConsta.KEY_Address, Myprefs.getString(MyConsta.KEY_Address, ""));
        Userinfo.put(MyConsta.KEY_City, Myprefs.getString(MyConsta.KEY_City, ""));
        Userinfo.put(MyConsta.KEY_BirthDate, Myprefs.getString(MyConsta.KEY_BirthDate, ""));
        Userinfo.put(MyConsta.KEY_ProfileImage, Myprefs.getString(MyConsta.KEY_ProfileImage, ""));
        Userinfo.put(MyConsta.KEY_PostalCode, Myprefs.getString(MyConsta.KEY_PostalCode, ""));
        Userinfo.put(MyConsta.KEY_Gender, String.valueOf(Myprefs.getInt(MyConsta.KEY_Gender, 0)));

        Userinfo.put(MyConsta.KEY_BIKE_ISAssigned,String.valueOf(Myprefs.getBoolean(MyConsta.KEY_CompanyID, false)) );
        Userinfo.put(MyConsta.KEY_BIKE_ID,Myprefs.getString(MyConsta.KEY_BIKE_ID, ""));
        Userinfo.put(MyConsta.KEY_BIKE_BRAND,Myprefs.getString(MyConsta.KEY_BIKE_BRAND, ""));
        Userinfo.put(MyConsta.KEY_BIKE_MODEL,Myprefs.getString(MyConsta.KEY_BIKE_MODEL, ""));
        Userinfo.put(MyConsta.KEY_BIKE_NAME,Myprefs.getString(MyConsta.KEY_BIKE_NAME, ""));
        Userinfo.put(MyConsta.KEY_BIKE_PATENT,Myprefs.getString(MyConsta.KEY_BIKE_PATENT, ""));

        Userinfo.put(MyConsta.KEY_CompanyID, Myprefs.getString(MyConsta.KEY_CompanyID, ""));
        Userinfo.put(MyConsta.KEY_CompanyFName, Myprefs.getString(MyConsta.KEY_CompanyFName, ""));
        Userinfo.put(MyConsta.KEY_CompanyLName, Myprefs.getString(MyConsta.KEY_CompanyLName, ""));
        Userinfo.put(MyConsta.KEY_CompanyFullName, Myprefs.getString(MyConsta.KEY_CompanyFullName, ""));


        return Userinfo;
    }

    public void LogOut() {

        Myprefs.clear();
        Myprefs.commit();

    }


    public int getMySeat_Raw() {
        return Myprefs.getInt(MyConsta.KEY_MYSEAT_RAW,0);
    }

    public void setMySeat_Raw(int mySeat_Raw) {
        Myprefs.putInt(MyConsta.KEY_MYSEAT_RAW,mySeat_Raw);
    }

    public int getMySeat_Col() {
        return Myprefs.getInt(MyConsta.KEY_MYSEAT_COL,0);
    }

    public void setMySeat_Col(int mySeat_Col) {
        Myprefs.putInt(MyConsta.KEY_MYSEAT_COL,mySeat_Col);
    }

   public void AnimateView(View view){

        if (view != null) {
            view.startAnimation(mAnimation);
        }


    }
    public void AnimateViewShake(View view){

        if (view != null) {
            view.startAnimation(mAnimationShake);
        }


    }

}
