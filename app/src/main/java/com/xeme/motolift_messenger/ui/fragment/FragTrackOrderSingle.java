package com.xeme.motolift_messenger.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.xeme.motolift_messenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragTrackOrderSingle#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragTrackOrderSingle extends Fragment  implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.ftos_actv_address_of_a)
    AppCompatTextView ftosActvAddressOfA;
    @BindView(R.id.ftos_actv_address_of_b)
    AppCompatTextView ftosActvAddressOfB;
    @BindView(R.id.ftos_actv_time)
    AppCompatTextView ftosActvTime;
    @BindView(R.id.ftos_mapview)
    MapView ftosMapview;
    GoogleMap mGoogleMap;
    LatLng mLatlong_a, mLatlong_b;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragTrackOrderSingle() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragTrackOrderSingle.
     */
    // TODO: Rename and change types and number of parameters
    public static FragTrackOrderSingle newInstance(String param1, String param2) {
        FragTrackOrderSingle fragment = new FragTrackOrderSingle();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_track_order_single, container, false);
        ButterKnife.bind(this, view);
        ftosMapview.onCreate(savedInstanceState);
        ftosMapview.getMapAsync(this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        GoogleMapOptions mapOptions = new GoogleMapOptions();
       // mapOptions.liteMode(true);

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        //  mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        // mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        // mGoogleMap.getUiSettings().setAllGesturesEnabled(false);

/*
        Marker mOrigin = mGoogleMap.addMarker(new MarkerOptions()
                .position(mLatlong_a)
                .title("Origin").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_a))
                .draggable(true));


        Marker mDestination = mGoogleMap.addMarker(new MarkerOptions()
                .position(mLatlong_b)
                .title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_b))
                .draggable(true));

        LatLngBounds.Builder bc = new LatLngBounds.Builder();
        bc.include(mOrigin.getPosition());
        bc.include(mDestination.getPosition());*/

        // CameraPosition cameraPosition = new CameraPosition.Builder().target(midPoint(40.4341661,-3.6740487, 40.4437812,-3.6700468)).zoom(14).bearing((float) angleBteweenCoordinate(40.4341661,-3.6740487, 40.4437812,-3.6700468)).build();

        // mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

      //  mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), MyHelper.dpToPx(MyHelper.getScreenWidth(getContext())), MyHelper.dpToPx(20), 20));


    }

    @Override
    public void onResume() {
        super.onResume();
        if (ftosMapview != null) {
            ftosMapview.onResume();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ftosMapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        ftosMapview.onLowMemory();
    }

}
