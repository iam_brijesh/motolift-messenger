package com.xeme.motolift_messenger.ui.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.adaps.MyDeliveryHistoryRVAdapter;
import com.xeme.motolift_messenger.pojos.Messengerorder;
import com.xeme.motolift_messenger.pojos.MyDeliveries;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragMyDeliveries extends Fragment implements MyDeliveryHistoryRVAdapter.MyDHRvHelper {

    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();
    @BindView(R.id.fmd_rv_list)
    RecyclerView fmdRvList;
    private OnFragmentInteractionListener mListener;
    List<Messengerorder> mDeliveryList;
        int mColumnCount = 1;
    View view;
    int MID;
    String UUID;
    GetProfileInfo mGetProfileInfo;
    Map<String, String> mUserInfo;

    public FragMyDeliveries() {
        // Required empty public constructor
    }

    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    void Init() {


        mGetProfileInfo = new GetProfileInfo(getContext());
        mUserInfo = mGetProfileInfo.getUserDetails();
        UUID = mUserInfo.get(MyConsta.KEY_UUID);
        MID = Integer.valueOf(mUserInfo.get(MyConsta.KEY_CID));
        GetOrderHistory();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag_my_deliveries, container, false);
        ButterKnife.bind(this, view);
        Init();
        return view;
    }

   /* // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //  void onFragmentInteraction(Uri uri);
    }


    void GetOrderHistory() {

        ShowDialog("Please wait... Getting Your order history");

        Call<MyDeliveries> call = service.DeliveryHistory(UUID, MID);

        call.enqueue(new Callback<MyDeliveries>() {
            @Override
            public void onResponse(Response<MyDeliveries> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {

                        mDeliveryList = response.body().messengerorders;


                        if (mDeliveryList != null && mDeliveryList.size() > 0) {

                            initRecycleView();

                        } else {
                            Toast.makeText(getActivity(), "No Order To display", Toast.LENGTH_SHORT).show();
                            Logwtf("GetOrderHistory", "No Order To display" + response.raw());
                        }
                        initRecycleView();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("GetOrderHistory", "Error" + response.raw());
                    Logwtf("GetOrderHistory", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();

            }
        });


    }

    void initRecycleView() {


        Context context = view.getContext();

        if (mColumnCount <= 1) {
            fmdRvList.setLayoutManager(new LinearLayoutManager(context));
        } else {
            fmdRvList.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        fmdRvList.setAdapter(new MyDeliveryHistoryRVAdapter (mDeliveryList, this, getActivity()));

        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        fmdRvList.setItemAnimator(mItemAnimator);

    }

    @Override
    public void OnEventSelected(String EventId) {

    }

    @Override
    public void OnClickRate(String id,int rated) {
        Dialog _dRate = new Dialog(getContext());
        _dRate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dRate.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _dRate.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        _dRate.setCancelable(true);
        _dRate.setContentView(R.layout.dialog_rate);
        AppCompatRatingBar mRatingBar = (AppCompatRatingBar) _dRate.findViewById(R.id.dr_ratebar);

        if(rated>0){
            mRatingBar.setRating(rated);
        }
        mRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            if (rating < 1.0f)
                ratingBar.setRating(1.0f);
        });
        AppCompatButton mSubmit = (AppCompatButton) _dRate.findViewById(R.id.dr_acbtn_submit);
        mSubmit.setOnClickListener(v -> {

            if (_dRate.isShowing()) {
                _dRate.cancel();
                RateUser(id, (int) mRatingBar.getRating());

            }
        });
        _dRate.show();

    }
    void RateUser(String OID, int Ratings) {

        ShowDialog("Updating Ratings...");
        Call<Simple_Response> Call = service.RateUSER(UUID, MID, OID, Ratings);
        Call.enqueue(new Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {

                        Toast.makeText(getActivity().getApplicationContext(), "Rated Successfully", Toast.LENGTH_SHORT).show();
                        GetOrderHistory();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("GetOrderHistory", "Error" + response.raw());
                    // Logwtf("GetOrderHistory", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(getActivity(), "Rate Updating Failed...Please Try Again later", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
