package com.xeme.motolift_messenger.ui.dialog;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by root on 31/8/16.
 */
public class MyProgressDialog {

    Context mContext;
    private ProgressDialog mProgressDialog;
    public static MyProgressDialog instance = null;

    public MyProgressDialog() {
    }

    public static MyProgressDialog getInstance() {
        if (null == instance) {
            instance = new MyProgressDialog();
        }
        return instance;
    }

    public MyProgressDialog(Context context) {
        mContext = context;
        mProgressDialog = new ProgressDialog(mContext);
    }

    public  void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }
}
