package com.xeme.motolift_messenger.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.utils.MyHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragLoginRegisterMain extends Fragment {

    @BindView(R.id.flrm_acbtn_register_with_email)
    AppCompatButton flrmAcbtnRegisterWithEmail;
    @BindView(R.id.flrm_actv_login)
    AppCompatTextView flrmActvLogin;
    @BindView(R.id.flrm_aciv_register_with_fb)
    AppCompatImageView flrmAcivRegisterWithFb;
    @BindView(R.id.flrm_aciv_register_with_google)
    AppCompatImageView flrmAcivRegisterWithGoogle;
    @BindView(R.id.flrm_aciv_register_with_linkedin)
    AppCompatImageView flrmAcivRegisterWithLinkedin;
    private OnFragmentInteractionListener mListener;
    ActionBar mActionBar;



    public FragLoginRegisterMain() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_login_register_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

       AppCompatActivity mActivity=(AppCompatActivity) context;
       mActionBar= mActivity.getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle("Welcome To MotoLift");
           // mActionBar.setDisplayHomeAsUpEnabled(true);
        }
     }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.flrm_acbtn_register_with_email, R.id.flrm_actv_login, R.id.flrm_aciv_register_with_fb, R.id.flrm_aciv_register_with_google, R.id.flrm_aciv_register_with_linkedin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.flrm_acbtn_register_with_email:
                if (mListener != null) {
                    mListener.swapFrags(new FragRegisterWithEmail());
                }
                break;
            case R.id.flrm_actv_login:
                if (mListener != null) {
                    mListener.swapFrags(new Frag_Login());
                }
                break;
            case R.id.flrm_aciv_register_with_fb:
                if (mListener != null) {
                    mListener.LoginWith(MyHelper.LOGIN_FACEBOOK);
                }

                break;
            case R.id.flrm_aciv_register_with_google:
                if (mListener != null) {
                    mListener.LoginWith(MyHelper.LOGIN_GOOGLEPLUS);
                }
                break;
            case R.id.flrm_aciv_register_with_linkedin:
                if (mListener != null) {
                    mListener.LoginWith(MyHelper.LOGIN_LINKEDIN);
                }
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void swapFrags(Fragment fragment);
        void LoginWith(int with);
    }


}
