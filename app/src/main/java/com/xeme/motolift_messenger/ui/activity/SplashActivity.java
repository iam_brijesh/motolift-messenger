package com.xeme.motolift_messenger.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.animation.AnimationUtils;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.components.MyLocationService;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.MyHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.as_aciv_logo)
    AppCompatImageView asAcivLogo;
    float imgSize;
    int size;
    MyDatahelper Myprefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imgSize = MyHelper.pxFromDp(SplashActivity.this, 300);
        size = (int) imgSize;
        ButterKnife.bind(this);
        Myprefs = MyDatahelper.getInstance(this, MyConsta.My_Pref);
        Picasso.with(SplashActivity.this).load(R.drawable.motolift_logo).resize(size, size).centerInside().into(asAcivLogo, new Callback() {
            @Override
            public void onSuccess() {
                asAcivLogo.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.shake));
            }

            @Override
            public void onError() {

            }
        });


        new Handler().postDelayed(() -> {

            Intent i = null;
            if (!Myprefs.getString(MyConsta.KEY_UUID, "").equals("") && Myprefs.getInt(MyConsta.KEY_CID, 0) > 0 && Myprefs.getBoolean(MyConsta.KEY_IsLoggedIn, false)) {

                i = new Intent(SplashActivity.this, HomeActivity.class);
                Intent Serviceintent = new Intent(SplashActivity.this, MyLocationService.class);
                if (!isMyServiceRunning(MyLocationService.class)) {
                    startService(Serviceintent);
                }


            } else {
                i = new Intent(SplashActivity.this, MainActivity.class);
            }

            startActivity(i);
            finish();
        }, 3000);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
