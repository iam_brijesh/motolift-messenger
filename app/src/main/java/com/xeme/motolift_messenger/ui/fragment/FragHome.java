package com.xeme.motolift_messenger.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.adaps.MyAvailebleOrdersRVAdapter;
import com.xeme.motolift_messenger.pojos.AvailableOrdersMain;
import com.xeme.motolift_messenger.pojos.Availableorder;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragHome extends Fragment implements MyAvailebleOrdersRVAdapter.MyAORvHelper {

    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();

    View view;
    int MID;
    String UUID;
    GetProfileInfo mGetProfileInfo;
    Map<String, String> mUserInfo;
    @BindView(R.id.fh_rv_list)
    RecyclerView fhRvList;
    List<Availableorder> mAvailableordersList;
    int mColumnCount = 1;


    private OnFragmentInteractionListener mListener;

    public FragHome() {
        // Required empty public constructor
    }


    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    void Init() {


        mGetProfileInfo = new GetProfileInfo(getContext());
        mUserInfo = mGetProfileInfo.getUserDetails();
        UUID = mUserInfo.get(MyConsta.KEY_UUID);
        MID = Integer.valueOf(mUserInfo.get(MyConsta.KEY_CID));

        GetAvailableorders();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag_home, container, false);
        ButterKnife.bind(this, view);
        Init();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


        AppCompatActivity mactivity=(AppCompatActivity) context;
        try {
            mactivity.getSupportActionBar().setTitle("Available Orders");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnOrderSelected(Availableorder mAvailableorder) {
        EventBus.getDefault().postSticky(mAvailableorder);
        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout).replace(((ViewGroup) getView().getParent()).getId(), new FragNewShipping())
                .addToBackStack(this.getTag())
                .commit();


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onFragmentInteraction(Uri uri);
    }




    void GetAvailableorders() {

        ShowDialog("Please wait... Getting Your order history");

        Call<AvailableOrdersMain> call = service.AvailableOrders(UUID, MID);

        call.enqueue(new Callback<AvailableOrdersMain>() {
            @Override
            public void onResponse(Response<AvailableOrdersMain> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {

                        mAvailableordersList = response.body().availableorders;


                        if (mAvailableordersList != null && mAvailableordersList.size() > 0) {

                            initRecycleView();

                        } else {
                            Toast.makeText(getContext(), "No Order To display", Toast.LENGTH_SHORT).show();
                            Logwtf("GetOrderHistory", "No Order To display" + response.raw());
                        }
                        initRecycleView();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("GetOrderHistory", "Error RAW 201" + response.raw());
                  //  Logwtf("GetOrderHistory", "Error BODY 201" +  (response.body().message==null ? "Null":response.body().message ));
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();

            }
        });


    }


    void initRecycleView() {


        Context context = view.getContext();

        if (mColumnCount <= 1) {
            fhRvList.setLayoutManager(new LinearLayoutManager(context));
        } else {
            fhRvList.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        fhRvList.setAdapter(new MyAvailebleOrdersRVAdapter(mAvailableordersList, this, getActivity()));

        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        fhRvList.setItemAnimator(mItemAnimator);

    }
}
