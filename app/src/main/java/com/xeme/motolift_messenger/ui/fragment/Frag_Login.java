package com.xeme.motolift_messenger.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Login_Response;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.ui.activity.ForgotPasswordActivity;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Frag_Login extends Fragment implements Validator.ValidationListener {
    @NotEmpty(trim = true)
    @Email()
    @BindView(R.id.fl_acet_username)
    AppCompatEditText flAcetUsername;

    @Password(min = 6, scheme = Password.Scheme.ANY, message = "minimum 6  char")
    @BindView(R.id.fl_acet_password)
    AppCompatEditText flAcetPassword;

    @BindView(R.id.fl_acbtn_login)
    AppCompatButton flAcbtnLogin;
    @BindView(R.id.fl_actv_forgot_password)
    AppCompatTextView flActvForgotPassword;
    private OnFragmentInteractionListener mListener;
    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();
    MyDatahelper MyDBHelper;
    Validator mValidator;
    public Frag_Login() {
        // Required empty public constructor
    }
    ActionBar mActionBar;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_login, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    void initViews(){
        MyDBHelper = MyDatahelper.getInstance(getContext(), MyConsta.My_Pref);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        AppCompatActivity mActivity=(AppCompatActivity) context;
        mActionBar= mActivity.getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle("Login");
            // mActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.fl_acbtn_login, R.id.fl_actv_forgot_password})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fl_acbtn_login:

                mValidator.validate();
                break;
            case R.id.fl_actv_forgot_password:
                Intent ForgotPasswordIntent=new Intent(getActivity(), ForgotPasswordActivity.class);
                getActivity().startActivity(ForgotPasswordIntent);

                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        DoLogin();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onFragmentLoginSuccess();
    }




    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }


    void DoLogin() {

        ShowDialog("Logging In...Please Wait");


        Call<Login_Response> call = service.GetLoginDetails(flAcetUsername.getText().toString(), flAcetPassword.getText().toString());
        String FB_Token= FirebaseInstanceId.getInstance().getToken();
        call.enqueue(new Callback<Login_Response>() {
            @Override
            public void onResponse(Response<Login_Response> response, Retrofit retrofit) {

                if (response.code() == 200) {
                    if (response != null) {

                        if (response.body().error.equals(false)) {



                            if(response.body().is_active==0){


                            Toast.makeText(getContext(), "Login Success", Toast.LENGTH_SHORT).show();
                            MyDBHelper.putBoolean(MyConsta.KEY_IsLoggedIn, true);
                            MyDBHelper.putString(MyConsta.KEY_Email, response.body().email);
                            MyDBHelper.putString(MyConsta.KEY_UUID, response.body().apikey);
                            MyDBHelper.putString(MyConsta.KEY_Name, response.body().name);
                            MyDBHelper.putString(MyConsta.KEY_Surname, response.body().surname);
                            MyDBHelper.putBoolean(MyConsta.KEY_KeepMeLoggedIn, true);
                            MyDBHelper.putInt(MyConsta.KEY_CID, response.body().mid);
                            MyDBHelper.putInt(MyConsta.KEY_REGISTEREDFROM, response.body().regifrom);
                            MyDBHelper.putString(MyConsta.KEY_Phone, response.body().mobile);
                            MyDBHelper.putString(MyConsta.KEY_ProfileImage, response.body().profileimage);
                            MyDBHelper.putString(MyConsta.KEY_FullName, response.body().name + " " + response.body().surname);
                            MyDBHelper.putString(MyConsta.KEY_City, response.body().cityName);
                            MyDBHelper.putString(MyConsta.KEY_PostalCode, String.valueOf(response.body().cityZipCode));

                            //company info
                            MyDBHelper.putBoolean(MyConsta.KEY_IsCompanyAssined, response.body().isCompanyAssined);

                            if (response.body().isCompanyAssined) {

                                MyDBHelper.putString(MyConsta.KEY_CompanyID, String.valueOf(response.body().companyID));
                                MyDBHelper.putString(MyConsta.KEY_CompanyFName, response.body().companyFisrtName);
                                MyDBHelper.putString(MyConsta.KEY_CompanyLName, response.body().companyLastName);
                                MyDBHelper.putString(MyConsta.KEY_CompanyFullName, response.body().companyFisrtName + " " + response.body().companyLastName);

                            }

                            MyDBHelper.putBoolean(MyConsta.KEY_BIKE_ISAssigned, response.body().isBikeAssigned);


                            if (response.body().isBikeAssigned){

                                MyDBHelper.putString(MyConsta.KEY_BIKE_ID, String.valueOf( response.body().bikeID));
                                MyDBHelper.putString(MyConsta.KEY_BIKE_BRAND, response.body().bikeBrand);
                                MyDBHelper.putString(MyConsta.KEY_BIKE_MODEL, response.body().bikeModel);
                                MyDBHelper.putString(MyConsta.KEY_BIKE_NAME, response.body().bikeName);
                                MyDBHelper.putString(MyConsta.KEY_BIKE_PATENT, response.body().bikePatent);


                            }

                            MyDBHelper.commit();

                            MyApplication.getInstance().UpdateNotifToken(response.body().mid,response.body().apikey,FB_Token);

                            if (mListener != null) {
                                mListener.onFragmentLoginSuccess();
                            }

                            }

                            else {

                                new AlertDialog.Builder(getContext())
                                        .setTitle("Account is Not Activated!!!")
                                        .setMessage("Your Account is not activated. please active your account by an email which has been sent to your at "+response.body().email+" when you are registered for this app")
                                        .setPositiveButton("Resend Activation Email.",(dialog, which) ->SendActivationRequest(response.body().email,response.body().apikey))
                                        .setNegativeButton("No. Received an email already",(dialog, which) ->dialog.dismiss())
                                        .setCancelable(false)
                                        .show();

                            }

                        } else {

                            Toast.makeText(getContext(), response.body().message, Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                else if(response.code()==201) {

                    Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message.toString());

                    Toast.makeText(getContext(), "Invalid Response From Server", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(), "Invalid Response", Toast.LENGTH_SHORT).show();
                }

                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(), "Something went Wrong Please try after some time ", Toast.LENGTH_SHORT).show();
                Logwtf("onFailure", t.getCause().toString());
                hideDialog();
            }
        });
    }



    void SendActivationRequest(String mEmail,String mApiKey)
    {


        Call<Simple_Response> REQ_ACTIVATION_CALL=service.SendActivationRequest(mApiKey,mEmail);
        REQ_ACTIVATION_CALL.enqueue(new Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    if (response != null) {

                        if (response.body().error.equals(false)) {

                            ShowActivationDialog(response.body().email);

                        } else {

                            Toast.makeText(getContext(), response.body().message, Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                else if(response.code()==201) {

                    Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message.toString());

                    Toast.makeText(getContext(), "Invalid Response From Server", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(), "Invalid Response", Toast.LENGTH_SHORT).show();
                }

                        }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }


    void ShowActivationDialog(String Email){


        LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView textView = new TextView(getContext());
        textView.setTypeface(MyApplication.GetFonts(MyConsta.Font_Light));
        textView.setText("Success");
        int value=MyHelper.dpToPx(14);
        // int value2=MyHelper.dpToPx(12);
        lp.setMargins(value,value,value,value);
        textView.setPadding(value,value,value,value);
        textView.setLayoutParams(lp);
        textView.setTextSize(18);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext()).setCustomTitle(textView)
                .setPositiveButton("Ok", (dialog, which) -> dialog.dismiss()).setCancelable(true);

        android.support.v7.app.AlertDialog dialog = builder.create();
        TextView textView2 = new TextView(getContext());

        textView2.setText(Html.fromHtml("An Email Has been sent to you at <b>"+Email+"</b>. Please Check You Email for further process of activation.Thank You"));
        textView2.setTypeface(MyApplication.GetFonts(MyConsta.Font_Thin));
        textView2.setPadding(value,value,value,value);
        dialog.setView(textView2);
        textView2.setLayoutParams(lp);
        textView2.setTextSize(16);
        dialog.show();

    }

}
