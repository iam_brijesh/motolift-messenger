package com.xeme.motolift_messenger.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xeme.motolift_messenger.CustomViews.CustomMapView;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Availableorder;
import com.xeme.motolift_messenger.pojos.Messengerorder;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragNewShipping extends Fragment implements OnMapReadyCallback {


    GoogleMap mGoogleMap;
    @BindView(R.id.fns_mapview)
    CustomMapView fnsMapview;
    @BindView(R.id.fns_actv_dtype)
    AppCompatTextView fnsActvDtype;
    @BindView(R.id.fns_actv_address_a)
    AppCompatTextView fnsActvAddressA;
    @BindView(R.id.fns_actv_contact_name_a)
    AppCompatTextView fnsActvContactNameA;
    @BindView(R.id.fns_actv_contact_phone_a)
    AppCompatTextView fnsActvContactPhoneA;
    @BindView(R.id.fns_actv_address_b)
    AppCompatTextView fnsActvAddressB;
    @BindView(R.id.fns_actv_contact_name_b)
    AppCompatTextView fnsActvContactNameB;
    @BindView(R.id.fns_actv_contact_phone_b)
    AppCompatTextView fnsActvContactPhoneB;
    @BindView(R.id.fns_actv_distance)
    AppCompatTextView fnsActvDistance;
    @BindView(R.id.fns_actv_price)
    AppCompatTextView fnsActvPrice;
    /* @BindView(R.id.fns_actv_time)
     AppCompatTextView fnsActvTime;*/
    @BindView(R.id.fns_actv_orderdatetime)
    AppCompatTextView fnsActvOrderdatetime;
    @BindView(R.id.fns_acbtn_cancel)
    AppCompatButton fnsAcbtnCancel;
    @BindView(R.id.fns_acbtn_accept)
    AppCompatButton fnsAcbtnAccept;
    private OnFragmentInteractionListener mListener;
    Availableorder mAvailableorder;


    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();
    List<Messengerorder> mDeliveryList;
    int mColumnCount = 1;
    View view;
    int MID;
    String UUID;
    GetProfileInfo mGetProfileInfo;
    Map<String, String> mUserInfo;


    public static final String DTYE_IMIDIATE = "Immediate Shipping";
    public static final String DTYE_DOD = "Dispatch On Day";

    public FragNewShipping() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.frag_new_shipping, container, false);
        ButterKnife.bind(this, view);
        mAvailableorder = (Availableorder) EventBus.getDefault().removeStickyEvent(Availableorder.class);


        fnsMapview.onCreate(savedInstanceState);
        fnsMapview.getMapAsync(this);
        initviews();
        return view;
    }

    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    private void initviews() {


        mGetProfileInfo = new GetProfileInfo(getContext());
        mUserInfo = mGetProfileInfo.getUserDetails();
        UUID = mUserInfo.get(MyConsta.KEY_UUID);
        MID = Integer.valueOf(mUserInfo.get(MyConsta.KEY_CID));
        fnsActvAddressA.setMovementMethod(new ScrollingMovementMethod());
        fnsActvAddressB.setMovementMethod(new ScrollingMovementMethod());
        if (mAvailableorder != null) {
                if (mAvailableorder.dtype.equals("0")) {
                fnsActvDtype.setText(DTYE_IMIDIATE);
                fnsActvDtype.setBackgroundColor(Color.parseColor("#FF4133"));
            } else {
                fnsActvDtype.setText(DTYE_DOD);
                fnsActvDtype.setBackgroundColor(Color.parseColor("#FF00C853"));
            }

            fnsActvAddressA.setText(mAvailableorder.start);
            fnsActvAddressB.setText(mAvailableorder.destination);


            fnsActvContactNameA.setText(mAvailableorder.contactpersonA);
            fnsActvContactNameB.setText(mAvailableorder.contactpesonB);

            fnsActvContactPhoneA.setText(mAvailableorder.mobileA);
            fnsActvContactPhoneB.setText(mAvailableorder.mobileA);


            fnsActvDistance.setText(mAvailableorder.distance);
            fnsActvPrice.setText(mAvailableorder.price);
            fnsActvOrderdatetime.setText(mAvailableorder.dateTime);


        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            //  mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        GoogleMapOptions mapOptions = new GoogleMapOptions();
        mapOptions.liteMode(true);

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        //  mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        // mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        // mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        LatLng origin = new LatLng(0.0, 0.0);
        LatLng destination = new LatLng(0.0, 0.0);

        if (mAvailableorder != null) {
                origin = new LatLng(Double.valueOf(mAvailableorder.latlong.origin.lat), Double.valueOf(mAvailableorder.latlong.origin._long));
                destination = new LatLng(Double.valueOf(mAvailableorder.latlong.destination.lat), Double.valueOf(mAvailableorder.latlong.destination._long));
            }


        Marker mOrigin = mGoogleMap.addMarker(new MarkerOptions()
                .position(origin)
                .title("Origin").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_a))
                .draggable(true));


        Marker mDestination = mGoogleMap.addMarker(new MarkerOptions()
                .position(destination)
                .title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_b))
                .draggable(true));

        LatLngBounds.Builder bc = new LatLngBounds.Builder();
        bc.include(mOrigin.getPosition());
        bc.include(mDestination.getPosition());

        // CameraPosition cameraPosition = new CameraPosition.Builder().target(midPoint(40.4341661,-3.6740487, 40.4437812,-3.6700468)).zoom(14).bearing((float) angleBteweenCoordinate(40.4341661,-3.6740487, 40.4437812,-3.6700468)).build();

        // mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

         mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), MyHelper.dpToPx(10)));


    }

    @Override
    public void onResume() {
        super.onResume();
        if (fnsMapview != null) {
            fnsMapview.onResume();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fnsMapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        fnsMapview.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        fnsMapview.onPause();

    }

    private LatLng midPoint(double lat1, double long1, double lat2, double long2) {

        return new LatLng((lat1 + lat2) / 2, (long1 + long2) / 2);

    }

    private double angleBteweenCoordinate(double lat1, double long1, double lat2,
                                          double long2) {

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng;

        return brng;
    }

    @OnClick({R.id.fns_acbtn_cancel, R.id.fns_acbtn_accept})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fns_acbtn_cancel:
                getActivity().onBackPressed();
                break;
            case R.id.fns_acbtn_accept:


                if (mAvailableorder != null) {
                    if (mAvailableorder.id!=null){
                        AcceptOrder(mAvailableorder.id);
                    }
                }

                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        // void onFragmentInteraction(Uri uri);
    }




    void AcceptOrder(String OrderID) {

        ShowDialog("Please wait... Accepting Order");

        Call<Simple_Response> call = service.AcceptOrder(UUID, MID,OrderID);

        call.enqueue(new Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {

                        Toast.makeText(getActivity(),"You Accepted Order Successfully", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("GetOrderHistory", "Error" + response.raw());
                    Logwtf("GetOrderHistory", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();

            }
        });


    }

}
