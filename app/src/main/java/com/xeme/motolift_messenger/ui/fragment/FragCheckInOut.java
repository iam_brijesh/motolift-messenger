package com.xeme.motolift_messenger.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.adaps.MyCheckInOutRVAdapter;
import com.xeme.motolift_messenger.pojos.Messengerorder;
import com.xeme.motolift_messenger.pojos.MyDeliveries;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragCheckInOut extends Fragment implements MyCheckInOutRVAdapter.MyCIORvHelper {

    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();
    View view;
    int MID;
    String UUID, BIKE_ID;
    GetProfileInfo mGetProfileInfo;
    Map<String, String> mUserInfo;
    @BindView(R.id.fcio_rv_list)
    RecyclerView fcioRvList;
    List<Messengerorder> mDeliveryList = new ArrayList<>();
    int mColumnCount = 1;
    private OnFragmentInteractionListener mListener;

    public FragCheckInOut() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag_check_in_out, container, false);
        ButterKnife.bind(this, view);
        Init();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            // mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnCheckIN(Messengerorder messengerorder) {
        DoCheckIn(messengerorder.id);
    }

    @Override
    public void OnCheckOUT(Messengerorder messengerorder) {
        DoCheckOut(messengerorder.id);
    }


    void DoCheckIn(String OrderID) {
        ShowDialog("Please wait...Checking In");
        Call<Simple_Response> CheckInOutCall = service.DoCheckIn(UUID, MID, OrderID, BIKE_ID);
        CheckInOutCall.enqueue(new Callback<Simple_Response>() {

            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {
                        Toast.makeText(getActivity(), response.body().message, Toast.LENGTH_SHORT).show();
                        GetCheckINOUT();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("DoCheckInCheckOut", "Error" + response.raw());
                    Logwtf("DoCheckInCheckOut", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Logwtf("DoCheckInCheckOut", "Error" + t.getCause());
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }
    void DoCheckOut(String OrderID) {
        ShowDialog("Please wait...Checking Out");
        Call<Simple_Response> CheckInOutCall = service.DoCheckOut(UUID, MID, OrderID, BIKE_ID);
        CheckInOutCall.enqueue(new Callback<Simple_Response>() {

            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {
                        Toast.makeText(getActivity(), response.body().message, Toast.LENGTH_SHORT).show();
                        GetCheckINOUT();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("DoCheckInCheckOut", "Error" + response.raw());
                    Logwtf("DoCheckInCheckOut", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Logwtf("DoCheckInCheckOut", "Error" + t.getCause());
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    void Init() {


        mGetProfileInfo = new GetProfileInfo(getContext());
        mUserInfo = mGetProfileInfo.getUserDetails();
        UUID = mUserInfo.get(MyConsta.KEY_UUID);
        MID = Integer.valueOf(mUserInfo.get(MyConsta.KEY_CID));
        BIKE_ID = mUserInfo.get(MyConsta.KEY_BIKE_ID);
        if (BIKE_ID != null) {
            BIKE_ID="23";
        }
        GetCheckINOUT();

    }

    void GetCheckINOUT() {
        mDeliveryList = new ArrayList<>();
        ShowDialog("Please wait... Getting Your order");

        Call<MyDeliveries> call = service.DeliveryHistory(UUID, MID);

        call.enqueue(new Callback<MyDeliveries>() {
            @Override
            public void onResponse(Response<MyDeliveries> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {


                        for (Messengerorder messengerorder : response.body().messengerorders) {

                            if (messengerorder.status.equals("1")||messengerorder.status.equals("2") ) {
                                mDeliveryList.add(messengerorder);
                            }

                        }

                        //mDeliveryList = response.body().messengerorders;

                        if (mDeliveryList != null && mDeliveryList.size() > 0) {

                            initRecycleView();

                        } else {
                            Toast.makeText(getActivity(), "No Order To display", Toast.LENGTH_SHORT).show();
                            Logwtf("GetOrderHistory", "No Order To display" + response.raw());
                        }
                        initRecycleView();
                    } else {

                        Toast.makeText(getActivity(), "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("GetOrderHistory", "Error" + response.raw());
                    Logwtf("GetOrderHistory", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(getActivity(), MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();

            }
        });


    }

    void initRecycleView() {


        Context context = view.getContext();

        if (mColumnCount <= 1) {
            fcioRvList.setLayoutManager(new LinearLayoutManager(context));
        } else {
            fcioRvList.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        fcioRvList.setAdapter(new MyCheckInOutRVAdapter(mDeliveryList, this, getActivity()));

        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        fcioRvList.setItemAnimator(mItemAnimator);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //  void onFragmentInteraction(Uri uri);
    }

}
