package com.xeme.motolift_messenger.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.motolift_messenger.pojos.Result;
import com.xeme.motolift_messenger.R;

import java.util.HashMap;
import java.util.List;

public class MyMenuRVAdapter extends RecyclerView.Adapter<MyMenuRVAdapter.ViewHolder> {

    private final List<Result> mValues;
    private final OnListFragmentInteractionListener mListener;
    String orderid;
    HashMap<Integer, Integer> totals = new HashMap<>();
    HashMap<Integer, Double> totalsPrice = new HashMap<>();
    int pos;
    Context mContext;
    private MyHelper myHelper;


    public MyMenuRVAdapter(List<Result> items, OnListFragmentInteractionListener listener, MyHelper myHelper, Context ctx) {
        mValues = items;
        mListener = listener;
        this.myHelper = myHelper;
        mContext = ctx;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frag_my_deliveries_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        pos = position;


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public interface MyHelper {
        public void onTotalChange(int Total, double TotalPrice);
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Result item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public Result mItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;


        }

        @Override
        public String toString() {
            return "";
        }
    }
}
