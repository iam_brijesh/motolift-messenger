package com.xeme.motolift_messenger.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.ui.dialog.MyProgressDialog;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

public class ForgotPasswordActivity extends AppCompatActivity implements Validator.ValidationListener{
    @Email(message = "Invalid Email")
    @BindView(R.id.fpa_actv_email)
    AppCompatEditText fpaActvEmail;
    @BindView(R.id.fpa_acbtn_reset)
    AppCompatButton fpaAcbtnReset;
    Validator mValidator;
    SlimApi service = MyApplication.getService();
    MyProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        InitViews();

    }

    private void InitViews() {
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        mDialog=new MyProgressDialog(ForgotPasswordActivity.this);
    }

    @OnClick(R.id.fpa_acbtn_reset)
    public void onClick() {
        mValidator.validate();
    }



    @Override
    public void onValidationSucceeded() {
        ResetPassword();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }



    void ResetPassword(){

        Context ctx=ForgotPasswordActivity.this;
        mDialog.ShowDialog("Please Wait... Processing your request !");
        String Email=fpaActvEmail.getText().toString().trim().toLowerCase();
        Call<Simple_Response> ResetCall=service.ForgotPassword(Email);
        ResetCall.enqueue(new Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {
                        fpaActvEmail.setText("");
                        ShowDialog(Email);
                    } else {

                        Toast.makeText(ctx,response.body().message , Toast.LENGTH_LONG).show();
                        Logwtf("ResetPassword", "Error" + response.body().message);

                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(ctx, MyConsta.onFailureMsg, Toast.LENGTH_LONG).show();
                    Logwtf("ResetPassword", "Error" + response.raw());

                }
                mDialog.hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(ctx, "Something went Wrong Please try after some time.. ", Toast.LENGTH_SHORT).show();
                Logwtf("ResetPassword onFailure", t.getCause().toString());
                mDialog.hideDialog();
            }
        });


    }


    void ShowDialog(String Email){


        LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView textView = new TextView(ForgotPasswordActivity.this);
        textView.setTypeface(MyApplication.GetFonts(MyConsta.Font_Light));
        textView.setText("Forgot Password");
        int value=MyHelper.dpToPx(14);
       // int value2=MyHelper.dpToPx(12);
        lp.setMargins(value,value,value,value);
        textView.setPadding(value,value,value,value);
        textView.setLayoutParams(lp);
        textView.setTextSize(18);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ForgotPasswordActivity.this).setCustomTitle(textView)
                .setPositiveButton("Ok", (dialog, which) -> dialog.dismiss()).setCancelable(true);

        android.support.v7.app.AlertDialog dialog = builder.create();
        TextView textView2 = new TextView(this);
        textView2.setText(Html.fromHtml("An Email Has been sent to you at <b>"+Email+"</b>. Please Check You Email for further process."));
        textView2.setTypeface(MyApplication.GetFonts(MyConsta.Font_Thin));
        textView2.setPadding(value,value,value,value);
        dialog.setView(textView2);
        textView2.setLayoutParams(lp);
        textView2.setTextSize(16);
        dialog.show();

    }

}
