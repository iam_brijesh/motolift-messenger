package com.xeme.motolift_messenger.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.squareup.picasso.Picasso;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.ui.fragment.FragCheckInOut;
import com.xeme.motolift_messenger.ui.fragment.FragHome;
import com.xeme.motolift_messenger.ui.fragment.FragMessengerProfile;
import com.xeme.motolift_messenger.ui.fragment.FragMyDeliveries;
import com.xeme.motolift_messenger.ui.fragment.FragNewShipping;
import com.xeme.motolift_messenger.utils.CircleTransformBorder;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.MyHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FragMyDeliveries.OnFragmentInteractionListener,
        FragNewShipping.OnFragmentInteractionListener,
        FragHome.OnFragmentInteractionListener,
        FragCheckInOut.OnFragmentInteractionListener,
        FragMessengerProfile.OnFragmentInteractionListener {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    NavigationView navView;
    Toolbar toolbar;
    DrawerLayout drawer;
    @BindView(R.id.frag_container)
    FrameLayout fragContainer;
    @BindView(R.id.ah_menu_aciv_profile)
    AppCompatImageView ahMenuAcivProfile;
    @BindView(R.id.ah_menu_actv_name)
    AppCompatTextView ahMenuActvName;
    @BindView(R.id.ah_menu_actv_subtext)
    AppCompatTextView ahMenuActvSubtext;
    @BindView(R.id.ah_menu_tv_ms)
    AppCompatTextView ahMenuTvMs;
    @BindView(R.id.ah_menu_tv_ci_co)
    AppCompatTextView ahMenuTvCiCo;
    @BindView(R.id.ah_menu_tv_score)
    AppCompatTextView ahMenuTvScore;
    @BindView(R.id.ah_menu_btn_invite)
    AppCompatButton ahMenuBtnInvite;
    @BindView(R.id.ah_menu_tv_logout)
    AppCompatTextView ahMenuTvLogout;
    MyDatahelper Myprefs;
    int imageSize = MyHelper.dpToPx(100);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Myprefs = MyDatahelper.getInstance(this, MyConsta.My_Pref);
        LoadUserInfo();
        InitViews();


    }


    private void InitViews() {

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.frag_container, new FragHome()).commit();


        if (toolbar != null) {
            getSupportFragmentManager().addOnBackStackChangedListener(() -> {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {


                    /*else {
                        getSupportActionBar().setHomeAsUpIndicator(backicon);
                    }*/

                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                    toolbar.setNavigationOnClickListener(v -> onBackPressed());
                } else {
                    //show hamburger
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    toolbar.setTitle("Available Orders");
                    toggle.syncState();
                    toolbar.setNavigationOnClickListener(v -> drawer.openDrawer(GravityCompat.START));
                }
            });

        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick({R.id.ah_menu_aciv_profile, R.id.ah_menu_actv_name, R.id.ah_menu_actv_subtext, R.id.ah_menu_tv_ms, R.id.ah_menu_tv_ci_co, R.id.ah_menu_tv_score, R.id.ah_menu_btn_invite, R.id.ah_menu_tv_logout})
    public void onClick(View view) {
        Fragment fragment = null;
        switch (view.getId()) {
            case R.id.ah_menu_aciv_profile:
                fragment = new FragMessengerProfile();
                toolbar.setTitle("My Profile");
                break;
            case R.id.ah_menu_actv_name:
                break;
            case R.id.ah_menu_actv_subtext:
                break;
            case R.id.ah_menu_tv_ms:
                fragment = new FragMyDeliveries();
                toolbar.setTitle("My Deliveries");
                break;
            case R.id.ah_menu_tv_ci_co:
                fragment = new FragCheckInOut();
                toolbar.setTitle("Check In/Check Out");
                break;
            case R.id.ah_menu_tv_score:
                break;
            case R.id.ah_menu_btn_invite:
                break;
            case R.id.ah_menu_tv_logout:
                LogOut();
                break;
        }


        if (fragment != null) {

            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout);
            mFragmentTransaction.replace(R.id.frag_container, fragment);
            mFragmentTransaction.addToBackStack(fragment.getTag());
            mFragmentTransaction.commit();
            this.onBackPressed();
        }
    }


    void LogOut() {

        new AlertDialog.Builder(this)
                .setTitle("Log Out")
                .setCancelable(false)
                .setIcon(R.drawable.ic_settings_power_red_48dp)
                .setMessage("Are You sure???")
                .setPositiveButton("Yes. Log out.", (dialog, which) -> {
                    Myprefs.clear();
                    Myprefs.commit();
                    //  mMyApplication.clearApplicationData();
                    Intent i = new Intent(HomeActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    this.finish();


                }).setNegativeButton("No.", (dialog, which) -> {
            dialog.dismiss();

        }).show();


    }

    @OnClick({R.id.ah_menu_btn_invite, R.id.ah_menu_tv_logout})
    public void onInvite(View view) {
        switch (view.getId()) {
            case R.id.ah_menu_btn_invite:
                Intent InviteIntetn = new Intent(HomeActivity.this, Activity_Invite.class);
                startActivity(InviteIntetn);
                break;
            case R.id.ah_menu_tv_logout:
                break;
        }
    }

    public void LoadUserInfo(){
        Picasso.with(HomeActivity.this).load(MyApplication.getProfileImage()).transform(new CircleTransformBorder()).resize(imageSize,imageSize).centerInside().into(ahMenuAcivProfile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Picasso.with(HomeActivity.this).load(R.drawable.default_profile_large).transform(new CircleTransformBorder()).resize(imageSize,imageSize).centerInside().into(ahMenuAcivProfile);
            }
        });
        ahMenuActvName.setText(MyApplication.getFullname());
    }
}
