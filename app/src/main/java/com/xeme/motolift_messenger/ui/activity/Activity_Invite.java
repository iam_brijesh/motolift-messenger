package com.xeme.motolift_messenger.ui.activity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.afollestad.assent.Assent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.adaps.MyAIRecyclerViewAdapter;
import com.xeme.motolift_messenger.pojos.Contact_Pojo;
import com.xeme.motolift_messenger.pojos.Invite_Request_pojo;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;


public class Activity_Invite extends AppCompatActivity implements MyAIRecyclerViewAdapter.MyRvHelper {

    int mColumnCount = 1;
    List<Contact_Pojo> mContactList;
    Observable<Void> GetUserContacts;
    Observable<Void> SendingInvitations;
    List<Contact_Pojo> MSelectedContactList;
    @BindView(R.id.ai_rv_contact)
    RecyclerView aiRvContact;
    @BindView(R.id.ai_acbtn_invite)
    AppCompatButton aiAcbtnInvite;
    SlimApi service = MyApplication.getService();
    GetProfileInfo mMy_helper;
    Map<String, String> UserProfile;
    ProgressDialog mProgressDialog;
    @BindView(R.id.aili_actv_empty)
    AppCompatTextView ailiActvEmpty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        ButterKnife.bind(this);
        Assent.setActivity(this, this);
        MSelectedContactList = new ArrayList<>();
        mMy_helper = new GetProfileInfo(Activity_Invite.this);
        UserProfile = mMy_helper.getUserDetails();
        CheckContactPermission();


    }


    public Void GetContactList() {
        Logwtf("GetContactList: ", "In GetContact");
        mContactList = new ArrayList<>();
        final String[] PROJECTION = new String[]{
                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Email.DATA
        };
        ContentResolver cr = getContentResolver();
        Cursor EmailsAndName = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");
       /* *//**//*Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);*//**//**/


        if (EmailsAndName != null) {
            final int contactIdIndex = EmailsAndName.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID);
            final int displayNameIndex = EmailsAndName.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            final int emailIndex = EmailsAndName.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);

            if (EmailsAndName.getCount() > 0) {

//                Toast.makeText(Activity_Invite.this, "TotalCols:" + EmailsAndName.getColumnCount(), Toast.LENGTH_SHORT).show();

                for (String s : EmailsAndName.getColumnNames()) {
                    Logwtf("GetContactList: ", "Col Name" + s);
                }

                while (EmailsAndName.moveToNext()) {
                    // Logwtf("Name:", EmailsAndName.getString(displayNameIndex));
                    //  Logwtf("Email:", EmailsAndName.getString(emailIndex));

                    if (EmailsAndName.getString(emailIndex) != null) {
                        mContactList.add(new Contact_Pojo(EmailsAndName.getLong(contactIdIndex), EmailsAndName.getString(displayNameIndex), EmailsAndName.getString(emailIndex), false));
                    }

                }

                Logwtf("Total Contacts With Emails", "" + mContactList.size());

            }
            EmailsAndName.close();
        }
        return null;
    }


    void CheckContactPermission() {
        if (MyHelper.isAndroid5()) {
            if (!Assent.isPermissionGranted(Assent.READ_CONTACTS)) {
                // The if statement checks if the permission has already been granted before
                Assent.requestPermissions(result -> {

                    if (result.allPermissionsGranted()) {
                        init();
                    } else {
                        RetryPermissionCheck();
                        Toast.makeText(Activity_Invite.this, "App need permission to Read Your Contact To send invitation", Toast.LENGTH_SHORT).show();
                    }
                }, 69, Assent.READ_CONTACTS);
            } else {
                init();
            }
        } else {
            init();
        }

    }


    private void RetryPermissionCheck() {

        new AlertDialog.Builder(this)
                .setTitle("Permission")
                .setCancelable(false)
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("App Need Permission To Read Your Contact to get Invitees. \nWant to Retry?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    CheckContactPermission();

                }).setNegativeButton("No", (dialog, which) -> {

            finish();

        }).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (GetUserContacts != null) {
            GetUserContacts.unsubscribeOn(Schedulers.newThread());
        }

    }
    @Override
    public void OnEventSelected(String EventId) {

    }

    @Override
    public void OnContactSelected(Contact_Pojo mContact_pojo, int Pos, boolean isSelected) {
        MSelectedContactList.get(Pos).setSelected(isSelected);
    }

    void initRecycleView() {


        Context context = Activity_Invite.this;

        if (mColumnCount <= 1) {
            aiRvContact.setLayoutManager(new LinearLayoutManager(context));
        } else {
            aiRvContact.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        aiRvContact.setAdapter(new MyAIRecyclerViewAdapter(mContactList, this, Activity_Invite.this));

        RecyclerView.ItemAnimator mItemAnimator = new DefaultItemAnimator();
        mItemAnimator.setAddDuration(500);
        mItemAnimator.setRemoveDuration(500);
        aiRvContact.setItemAnimator(mItemAnimator);

    }

    @OnClick(R.id.ai_acbtn_invite)
    public void onClick() {
        int i = 0;
        for (Contact_Pojo contact_pojo : mContactList) {
            if (contact_pojo.isSelected()) {
                i++;
            }
        }
        Toast.makeText(Activity_Invite.this, "Total Selected Emails:" + i, Toast.LENGTH_SHORT).show();
        if (i >= 1) {
            SendInvitations();
        } else {
            Toast.makeText(Activity_Invite.this, "You selected nothing", Toast.LENGTH_SHORT).show();
        }


    }


    void init() {
        GetUserContacts = Observable.defer(() -> Observable.just(GetContactList()));
        GetUserContacts.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {

                if (mContactList != null) {
                    if (mContactList.size() >= 1) {//mContactList.size()
                        MSelectedContactList = mContactList;
                        aiAcbtnInvite.setVisibility(View.VISIBLE);
                        if (ailiActvEmpty.getVisibility() == View.VISIBLE) {
                            ailiActvEmpty.setVisibility(View.GONE);
                        }
                        initRecycleView();
                    } else {
                        if (ailiActvEmpty.getVisibility() != View.VISIBLE) {
                            ailiActvEmpty.setVisibility(View.VISIBLE);
                            aiAcbtnInvite.setVisibility(View.GONE);
                        }

                    }
                } else {

                    if (ailiActvEmpty.getVisibility() != View.VISIBLE) {
                        ailiActvEmpty.setVisibility(View.VISIBLE);
                        aiAcbtnInvite.setVisibility(View.GONE);
                    }
                }
                Toast.makeText(Activity_Invite.this, "Completed", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Void aVoid) {

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        Assent.setActivity(this, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (this.isFinishing())
            Assent.setActivity(this, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Lets Assent handle permission results, and contact your callbacks
        Assent.handleResult(permissions, grantResults);
    }

    void SendInvitations() {

        Context ctx = Activity_Invite.this;
        ShowDialog("Please wait... Sending Your Invitations");
        List<Contact_Pojo> temp = new ArrayList<>();
        for (Contact_Pojo contact_pojo : mContactList) {
            if (contact_pojo.isSelected()) {
                temp.add(contact_pojo);
            }
        }

        // Toast.makeText(Activity_Invite.this, "mContactList:" + temp.size(), Toast.LENGTH_SHORT).show();

        Invite_Request_pojo mInviteRequestPojo = new Invite_Request_pojo(UserProfile.get(MyConsta.KEY_CID), UserProfile.get(MyConsta.KEY_UUID), temp);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String DemoInvitations = gson.toJson(mInviteRequestPojo);
        Logwtf("Invite Json: order", DemoInvitations);

        Call<Simple_Response> mServiceCall = service.SendInvitaions(mInviteRequestPojo);
        mServiceCall.enqueue(new Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (!response.body().error) {

                        Toast.makeText(ctx, "Success!!! Invitations will be delivered soon :)", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {

                        Toast.makeText(ctx, "Error:" + response.body().message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //Toast.makeText(getActivity(),"No Order To display", Toast.LENGTH_SHORT).show();
                    Toast.makeText(ctx, MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                    Logwtf("SendInvitations", "Error" + response.raw());
                    //  Logwtf("GetOrderHistory", "Error" + response.body().message);
                }
                hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                hideDialog();
                Toast.makeText(ctx, MyConsta.onFailureMsg, Toast.LENGTH_SHORT).show();
                Logwtf("SendInvitations", "Error" + t.getMessage());

            }
        });
    }

    void ShowDialog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(Activity_Invite.this);
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }
}


