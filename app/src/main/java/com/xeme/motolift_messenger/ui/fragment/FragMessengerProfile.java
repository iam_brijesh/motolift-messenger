package com.xeme.motolift_messenger.ui.fragment;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.assent.Assent;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.squareup.picasso.Picasso;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.ui.activity.HomeActivity;
import com.xeme.motolift_messenger.utils.CircleTransformBorder;
import com.xeme.motolift_messenger.utils.GetProfileInfo;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;


public class FragMessengerProfile extends Fragment implements Validator.ValidationListener {
    public static boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    @BindView(R.id.fup_aciv_profile_image)
    AppCompatImageView fupAcivProfileImage;
    @BindView(R.id.fup_actv_change_photo)
    AppCompatTextView fupActvChangePhoto;
    @BindView(R.id.fup_acet_name)
    AppCompatEditText fupAcetName;
    @BindView(R.id.fup_acet_surname)
    AppCompatEditText fupAcetSurname;
    @BindView(R.id.fup_acet_email)
    AppCompatEditText fupAcetEmail;
    @BindView(R.id.fup_acet_mobile)
    AppCompatEditText fupAcetMobile;
    @BindView(R.id.fup_acet_m_brand)
    AppCompatEditText fupAcetMBrand;
    @BindView(R.id.fup_acet_m_model)
    AppCompatEditText fupAcetMModel;
    @BindView(R.id.fup_acet_m_patent)
    AppCompatEditText fupAcetMPatent;
    @BindView(R.id.fup_acet_b_address)
    AppCompatEditText fupAcetBAddress;
    @BindView(R.id.fup_acet_b_name)
    AppCompatEditText fupAcetBName;
    @BindView(R.id.fup_acet_b_taxno)
    AppCompatEditText fupAcetBTaxno;
    @BindView(R.id.fup_acbtn_save)
    AppCompatButton fupAcbtnSave;
    @BindView(R.id.fup_acet_zipcode)
    AppCompatEditText fupAcetZipcode;
    @BindView(R.id.fup_acet_city)
    AppCompatEditText fupAcetCity;
    @BindView(R.id.fup_acet_company)
    AppCompatEditText fupAcetCompany;
    int CID;
    String UUID;
    ProgressDialog mProgressDialog;
    GetProfileInfo mGetProfileInfo;
    SlimApi service = MyApplication.getService();
    Map<String, String> mUserInfo;
    // MyProgressDialog mDialog;
    String mEmail;
    MyDatahelper mMyDatahelper;
    Validator mValidator;
    int imageSize = MyHelper.dpToPx(100);
    int imageSize4Upload = MyHelper.dpToPx(200);
    String strImagePath = "";
    String encodeImage = "null";
    Observable<String> GetImage;
    Handler mHandler;
    private OnFragmentInteractionListener mListener;

    public FragMessengerProfile() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_messenger_profile, container, false);
        ButterKnife.bind(this, view);
        mMyDatahelper = MyDatahelper.getInstance(getContext(), MyConsta.My_Pref);
        Init();
        Assent.setFragment(this, this);
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                // This is where you do your work in the UI thread.
                // Your worker tells you in the message what to do.
                Toast.makeText(getContext(), "Uploading Your Image..Please Wait..", Toast.LENGTH_LONG).show();
            }
        };
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        Assent.setFragment(this, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null && getActivity().isFinishing())
            Assent.setFragment(this, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Assent.handleResult(permissions, grantResults);
    }


    void Init() {


        GetImage = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(strImagePath);
            }
        });


        fupActvChangePhoto.setOnClickListener(v -> {

            if (MyHelper.isAndroid5()) {
                if (!Assent.isPermissionGranted(Assent.WRITE_EXTERNAL_STORAGE)) {
                    // The if statement checks if the permission has already been granted before
                    Assent.requestPermissions(result -> {

                        if (result.allPermissionsGranted()) {
                            choosepic();
                        } else {
                            Toast.makeText(getContext(), "App need permission to set profile picture", Toast.LENGTH_SHORT).show();
                        }
                    }, 69, Assent.WRITE_EXTERNAL_STORAGE, Assent.READ_EXTERNAL_STORAGE);
                } else {
                    choosepic();
                }
            } else {
                choosepic();
            }

        });


        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        //  mDialog = new MyProgressDialog(getContext());
        mGetProfileInfo = new GetProfileInfo(getContext());
        mUserInfo = mGetProfileInfo.getUserDetails();
        UUID = mUserInfo.get(MyConsta.KEY_UUID);
        CID = Integer.valueOf(mUserInfo.get(MyConsta.KEY_CID));
        mEmail = mUserInfo.get(MyConsta.KEY_Email);
        FillUserInfo();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            // mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.fup_acbtn_save)
    public void onClick() {
    }

    void FillUserInfo() {
        mUserInfo = mGetProfileInfo.getUserDetails();
        if (mUserInfo != null) {
            String mName = mUserInfo.get(MyConsta.KEY_Name);
            String mSurname = mUserInfo.get(MyConsta.KEY_Surname);
            String mEmail = mUserInfo.get(MyConsta.KEY_Email);
            String mMobile1 = mUserInfo.get(MyConsta.KEY_Phone);
            String mZipcode = mUserInfo.get(MyConsta.KEY_PostalCode);
            String mCity = mUserInfo.get(MyConsta.KEY_City);
            String mCompany = mUserInfo.get(MyConsta.KEY_CompanyFullName);
            boolean IsBikeAssigned = Boolean.valueOf(mUserInfo.get(MyConsta.KEY_BIKE_ISAssigned));

            String mBikeBrand = mUserInfo.get(MyConsta.KEY_BIKE_BRAND);
            String mBikeModel = mUserInfo.get(MyConsta.KEY_BIKE_MODEL);
            String mBikePatent = mUserInfo.get(MyConsta.KEY_BIKE_PATENT);


            fupAcetName.setText(mName);
            fupAcetSurname.setText(mSurname);
            fupAcetEmail.setText(mEmail);
            fupAcetMobile.setText(mMobile1);
            fupAcetZipcode.setText(mZipcode);
            fupAcetCity.setText(mCity);
            fupAcetCompany.setText(mCompany);


            fupAcetMBrand.setText(mBikeBrand);
            fupAcetMModel.setText(mBikeModel);
            fupAcetMPatent.setText(mBikePatent);


            Picasso.with(getActivity().getApplicationContext()).load(MyApplication.getProfileImage()).transform(new CircleTransformBorder()).resize(imageSize, imageSize).centerInside().into(fupAcivProfileImage, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(getActivity().getApplicationContext()).load(R.drawable.default_profile_large).transform(new CircleTransformBorder()).resize(imageSize, imageSize).centerInside().into(fupAcivProfileImage);
                }
            });

            fupAcetEmail.setOnClickListener(v -> {
                Toast.makeText(getContext(), "You can't edit Your Email Address.", Toast.LENGTH_SHORT).show();
            });
            fupAcetCompany.setOnClickListener(v -> {
                Toast.makeText(getContext(), "You can't edit Your Company.", Toast.LENGTH_SHORT).show();
            });

            fupAcetMBrand.setOnClickListener(v -> {
                Toast.makeText(getContext(), "You can't edit Your Company.", Toast.LENGTH_SHORT).show();
            });
            fupAcetMModel.setOnClickListener(v -> {
                Toast.makeText(getContext(), "You can't edit Your Company.", Toast.LENGTH_SHORT).show();
            });
            fupAcetMPatent.setOnClickListener(v -> {
                Toast.makeText(getContext(), "You can't edit Your Company.", Toast.LENGTH_SHORT).show();
            });


        }
    }

    @Override
    public void onValidationSucceeded() {
        // DoUpdateProfile();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @TargetApi(19)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getData() != null && resultCode == -1) {

            boolean isImageFromGoogleDrive = false;
            Uri uri = data.getData();

            if (isKitKat && DocumentsContract.isDocumentUri(getContext(), uri)) {

                if ("com.android.externalstorage.documents".equals(uri.getAuthority())) {
                    String docId = DocumentsContract.getDocumentId(uri);
                    String[] split = docId.split(":");
                    String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        strImagePath = Environment.getExternalStorageDirectory() + "/" + split[1];
                    } else {
                        Pattern DIR_SEPORATOR = Pattern.compile("/");
                        Set<String> rv = new HashSet<>();
                        String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
                        String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
                        String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
                        if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
                            if (TextUtils.isEmpty(rawExternalStorage)) {
                                rv.add("/storage/sdcard0");
                            } else {
                                rv.add(rawExternalStorage);
                            }
                        } else {
                            String rawUserId;
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                rawUserId = "";
                            } else {
                                String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                                String[] folders = DIR_SEPORATOR.split(path);
                                String lastFolder = folders[folders.length - 1];
                                boolean isDigit = false;
                                try {
                                    Integer.valueOf(lastFolder);
                                    isDigit = true;
                                } catch (NumberFormatException ignored) {
                                }
                                rawUserId = isDigit ? lastFolder : "";
                            }
                            if (TextUtils.isEmpty(rawUserId)) {
                                rv.add(rawEmulatedStorageTarget);
                            } else {
                                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
                            }
                        }
                        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
                            String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
                            Collections.addAll(rv, rawSecondaryStorages);
                        }
                        String[] temp = rv.toArray(new String[rv.size()]);

                        for (int i = 0; i < temp.length; i++) {
                            File tempf = new File(temp[i] + "/" + split[1]);
                            if (tempf.exists()) {
                                strImagePath = temp[i] + "/" + split[1];
                            }
                        }
                    }
                } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                    String id = DocumentsContract.getDocumentId(uri);
                    Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    Cursor cursor = null;
                    String column = "_data";
                    String[] projection = {
                            column
                    };

                    try {
                        cursor = getActivity().getContentResolver().query(contentUri, projection, null, null,
                                null);
                        if (cursor != null && cursor.moveToFirst()) {
                            final int column_index = cursor.getColumnIndexOrThrow(column);
                            strImagePath = cursor.getString(column_index);
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                } else if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                    String docId = DocumentsContract.getDocumentId(uri);
                    String[] split = docId.split(":");
                    String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    String selection = "_id=?";
                    String[] selectionArgs = new String[]{
                            split[1]
                    };

                    Cursor cursor = null;
                    String column = "_data";
                    String[] projection = {
                            column
                    };

                    try {
                        cursor = getActivity().getContentResolver().query(contentUri, projection, selection, selectionArgs,
                                null);
                        if (cursor != null && cursor.moveToFirst()) {
                            int column_index = cursor.getColumnIndexOrThrow(column);
                            strImagePath = cursor.getString(column_index);
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                } else if ("com.google.android.apps.docs.storage".equals(uri.getAuthority())) {
                    isImageFromGoogleDrive = true;
                }
            } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                Cursor cursor = null;
                String column = "_data";
                String[] projection = {
                        column
                };

                try {
                    cursor = getActivity().getContentResolver().query(uri, projection, null, null,
                            null);
                    if (cursor != null && cursor.moveToFirst()) {
                        int column_index = cursor.getColumnIndexOrThrow(column);
                        strImagePath = cursor.getString(column_index);
                    }
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                strImagePath = uri.getPath();
            }


            if (isImageFromGoogleDrive) {
                try {
                 /*   Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                    //  getDrawQR(bitmap);
                    Logwtf(" onActivity Result,Image Path", "ImagePath:=" + strImagePath + "\nBITE:" + bitmap.getByteCount() / 1024 + "\nRAwBITE:" + bitmap.getRowBytes() / 1024);
                  */
                    LoadPic("file://" + strImagePath);
                    //iv.setImageBitmap(bitmap);
                    //  tvImagePath.setText(getResources().getString(R.string.str_image_google_drive));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {


               /* File f = new File(strImagePath);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
                //getDrawQR(bitmap);
                Logwtf(" onActivity Result,Image Path", "ImagePath:=" + strImagePath + "\nBITE:" + bitmap.getByteCount() / 1024 + "\nRAwBITE:" + bitmap.getRowBytes() / 1024);
               */
                LoadPic("file://" + strImagePath);
                //iv.setImageBitmap(bitmap);
                // tvImagePath.setText(getResources().getString(R.string.str_image_path) + ": " + strImagePath);
            }


        }
        // super.onActivityResult(requestCode, resultCode, data);
    }

    void LoadPic(final String Path) {


        Picasso.with(getActivity().getApplicationContext()).load(Path).transform(new CircleTransformBorder()).resize(imageSize, imageSize).centerInside().into(fupAcivProfileImage);
        GetImage.subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                        Logwtf("onCompleted", "*-*-*Task Done*-*-");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logwtf("onError", "*-*-*ERRoR e*-*-" + e.toString());
                    }

                    @Override
                    public void onNext(String s) {
                        Logwtf("onNext", "file :" + s);

                        Bitmap bitmap = null;
                        try {
                            bitmap = Picasso.with(getActivity().getApplicationContext()).load("file://" + s).resize(imageSize4Upload, imageSize4Upload).centerInside().get();

                        } catch (IOException e) {
                            Logwtf("IOException", "Error :" + e);

                        } finally {
                            if (bitmap != null) {
                                //do whatever you wanna do with the picture.
                                //for me it was using my own cache
                                {
                                    // getDrawQR(bitmap);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                    encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                                    Logwtf("finally", "file in BASE64" + encodeImage.substring(0, 100));
                                    uploadImage(encodeImage);

                                }
                            } else {
                                Logwtf("finally", "Bitmap Null");
                            }
                        }

                    }
                });

    }

    void choosepic() {


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            FragMessengerProfile.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
        }

    }

    void uploadImage(String BimapString) {


        Message message = mHandler.obtainMessage(1, "Uploading Your Image..Please Wait");
        message.sendToTarget();

        // MyApplication.ShowDialog("Uploading Image...Please Wait...");

        Call<Simple_Response> call = service.UpdateProfileImage(MyApplication.getUUID(), MyApplication.getUID(), BimapString);
        call.enqueue(new retrofit.Callback<Simple_Response>() {
            @Override
            public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {
                    if (response.body().error.equals(false)) {

                        Toast.makeText(getActivity().getApplicationContext(), "Uploaded Successfully", Toast.LENGTH_LONG).show();
                        mMyDatahelper.putString(MyConsta.KEY_ProfileImage, response.body().img);
                        Picasso.with(getActivity().getApplicationContext()).load(response.body().img).transform(new CircleTransformBorder()).resize(imageSize, imageSize).centerInside().into(fupAcivProfileImage, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(getActivity().getApplicationContext()).load(R.drawable.default_profile_large).transform(new CircleTransformBorder()).resize(imageSize, imageSize).centerInside().into(fupAcivProfileImage);
                            }
                        });
                        ((HomeActivity) getActivity()).LoadUserInfo();

                    } else {
                        Toast.makeText(getContext(), "Uploaded Fail", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Invalid Response", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Logwtf("onFailure", "UPLOAD fail" + t.toString());
                Toast.makeText(getActivity().getApplicationContext(), "Uploaded Fail", Toast.LENGTH_LONG).show();

            }
        });

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //  void onFragmentInteraction(Uri uri);
    }

}
