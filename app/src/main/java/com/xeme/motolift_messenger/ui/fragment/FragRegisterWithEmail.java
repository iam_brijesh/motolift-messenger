package com.xeme.motolift_messenger.ui.fragment;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.assent.Assent;
import com.afollestad.assent.AssentCallback;
import com.afollestad.assent.PermissionResultSet;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.squareup.picasso.Picasso;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.adaps.MyCompanySpAdapter;
import com.xeme.motolift_messenger.components.SocialMediaProvider;
import com.xeme.motolift_messenger.pojos.Cities;
import com.xeme.motolift_messenger.pojos.City;
import com.xeme.motolift_messenger.pojos.Login_Response;
import com.xeme.motolift_messenger.pojos.UserAccount;
import com.xeme.motolift_messenger.utils.CircleTransformBorder;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;
import com.xeme.motolift_messenger.utils.MyHelper;
import com.xeme.motolift_messenger.utils.SlimApi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/*import com.rogalabs.lib.SocialUser;*/

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragRegisterWithEmail extends Fragment implements Validator.ValidationListener {

    public static boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    @Email(message = "Please Enter Valid Email")
    @BindView(R.id.frwe_acet_email)
    AppCompatEditText frweAcetEmail;
    @Order(4)
    @Length(min = 10, message = "Please Enter Valid Phone Number  Eg:\"9876543210\" it should be 10 digit long ")
    @BindView(R.id.frwe_acet_mobile)
    AppCompatEditText frweAcetMobile;
    @Password(min = 6, scheme = Password.Scheme.ANY, message = "minimum 6  char Eg:\"password\" ")
    @BindView(R.id.frwe_acet_password)
    AppCompatEditText frweAcetPassword;
    @NotEmpty(message = "Please Enter Name")
    @BindView(R.id.frwe_acet_name)
    AppCompatEditText frweAcetName;
    @NotEmpty(message = "Please Enter Surname")
    @BindView(R.id.frwe_acet_surname)
    AppCompatEditText frweAcetSurname;
    @BindView(R.id.frwe_aciv_profile)
    AppCompatImageView frweAcivProfile;
    @BindView(R.id.frwe_acibtn_choose)
    AppCompatImageView frweAcibtnChoose;
    @BindView(R.id.frwe_acbtn_continue)
    AppCompatButton frweAcbtnContinue;
    Validator mValidator;
    int imgsize = MyHelper.dpToPx(120);
    String strImagePath = "";
    String encodeImage = "";
    Bundle b;
    ActionBar mActionBar;
    Observable<String> GetImage;
    ProgressDialog mProgressDialog;
    SlimApi service = MyApplication.getService();
    MyDatahelper MyDBHelper;
    int imageSoze = MyHelper.dpToPx(200);
    @NotEmpty(trim = true, message = "Please Enter City")
    @BindView(R.id.frwe_acet_city)
    AppCompatEditText frweAcetCity;

    @Length(min = 5, message = "Please Enter Valid Postal Code eg:  \"08001\" for  Barcelona - el Raval ")
    @BindView(R.id.frwe_acet_postalcode)
    AppCompatEditText frweAcetPostalcode;
    UserAccount mUserAccount;
    @BindView(R.id.frwe_acsp_company)
    AppCompatSpinner frweAcspCompany;
    MyCompanySpAdapter mListAdapter;
    List<City> mCitiesList;
    SocialMediaProvider mProvider;
    int mIsRegiFrom = 3;
    int mSelectedCity = 0;
    private OnFragmentInteractionListener mListener;

    public FragRegisterWithEmail() {
        // Required empty public constructor
    }

    public static FragRegisterWithEmail newInstance(UserAccount userAccount, SocialMediaProvider Provider) {
        FragRegisterWithEmail fragment = new FragRegisterWithEmail();
        Bundle args = new Bundle();
        args.putParcelable(MyConsta.KEY_USER_ACCOUNT, userAccount);
        args.putSerializable("profile", Provider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        Assent.setFragment(this, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null && getActivity().isFinishing())
            Assent.setFragment(this, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Assent.handleResult(permissions, grantResults);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mUserAccount = getArguments().getParcelable(MyConsta.KEY_USER_ACCOUNT);
            mProvider = (SocialMediaProvider) getArguments().getSerializable("profile");

            switch (mProvider) {
                case FACEBOOK:
                    mIsRegiFrom = MyConsta.Login_Consta_Facebook;
                    break;
                case GOOGLE_PLUS:
                    mIsRegiFrom = MyConsta.Login_Consta_Google;
                    break;
                case LINKED_IN:
                    mIsRegiFrom = MyConsta.Login_Consta_Linkedin;
                    break;

            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.frag_register_with_email, container, false);
        ButterKnife.bind(this, view);
        MyDBHelper = MyDatahelper.getInstance(getContext(), MyConsta.My_Pref);
        initViews();
        Assent.setFragment(this, this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    void initViews() {

        if (mUserAccount != null) {
            if (!mUserAccount.getFullName().equals("")) {
                String str = mUserAccount.getFullName();
                String[] splited = str.split("\\s+");
                if (splited.length >= 1) {
                    frweAcetName.setText(splited[0]);
                    frweAcetSurname.setText(splited[1]);
                }
            }
            if (mUserAccount.getEmail().length() > 1) {
                frweAcetEmail.setText(mUserAccount.getEmail());
            }
            if (mUserAccount.getPictureURL() != null) {

                if (mUserAccount.getPictureURL().length() > 1) {
                    Picasso.with(getActivity().getApplicationContext()).load(mUserAccount.getPictureURL()).transform(new CircleTransformBorder()).resize(imageSoze, imageSoze).centerInside().into(frweAcivProfile);
                }

            }

        }
        GetImage = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(strImagePath);
            }
        });
        getCities();
        frweAcspCompany.setSelected(false);

        frweAcspCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mCitiesList != null) {
                    if (mCitiesList.size() > 0) {
                         // Toast.makeText(getContext(), mCitiesList.get(position).name, Toast.LENGTH_SHORT).show();
                        mSelectedCity = Integer.valueOf(mCitiesList.get(position).id);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        AppCompatActivity mActivity = (AppCompatActivity) context;
        mActionBar = mActivity.getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle("Registration");
            // mActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.frwe_acibtn_choose, R.id.frwe_acbtn_continue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frwe_acibtn_choose:


                if (MyHelper.isAndroid5()) {
                    if (!Assent.isPermissionGranted(Assent.WRITE_EXTERNAL_STORAGE)) {
                        // The if statement checks if the permission has already been granted before
                        Assent.requestPermissions(new AssentCallback() {
                            @Override
                            public void onPermissionResult(PermissionResultSet result) {

                                if (result.allPermissionsGranted()) {
                                    choosepic();
                                } else {
                                    Toast.makeText(getContext(), "App need permission to set profile picture", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 69, Assent.WRITE_EXTERNAL_STORAGE, Assent.READ_EXTERNAL_STORAGE);
                    } else {
                        choosepic();
                    }
                } else {
                    choosepic();
                }

                break;
            case R.id.frwe_acbtn_continue:
                mValidator.validate();
                break;
        }
    }


    void choosepic() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            FragRegisterWithEmail.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
        }

    }

    @Override
    public void onValidationSucceeded() {
        DoRegister();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @TargetApi(19)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getData() != null && resultCode == -1) {

            boolean isImageFromGoogleDrive = false;
            Uri uri = data.getData();

            if (isKitKat && DocumentsContract.isDocumentUri(getContext(), uri)) {

                if ("com.android.externalstorage.documents".equals(uri.getAuthority())) {
                    String docId = DocumentsContract.getDocumentId(uri);
                    String[] split = docId.split(":");
                    String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        strImagePath = Environment.getExternalStorageDirectory() + "/" + split[1];
                    } else {
                        Pattern DIR_SEPORATOR = Pattern.compile("/");
                        Set<String> rv = new HashSet<>();
                        String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
                        String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
                        String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
                        if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
                            if (TextUtils.isEmpty(rawExternalStorage)) {
                                rv.add("/storage/sdcard0");
                            } else {
                                rv.add(rawExternalStorage);
                            }
                        } else {
                            String rawUserId;
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                rawUserId = "";
                            } else {
                                String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                                String[] folders = DIR_SEPORATOR.split(path);
                                String lastFolder = folders[folders.length - 1];
                                boolean isDigit = false;
                                try {
                                    Integer.valueOf(lastFolder);
                                    isDigit = true;
                                } catch (NumberFormatException ignored) {
                                }
                                rawUserId = isDigit ? lastFolder : "";
                            }
                            if (TextUtils.isEmpty(rawUserId)) {
                                rv.add(rawEmulatedStorageTarget);
                            } else {
                                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
                            }
                        }
                        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
                            String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
                            Collections.addAll(rv, rawSecondaryStorages);
                        }
                        String[] temp = rv.toArray(new String[rv.size()]);

                        for (int i = 0; i < temp.length; i++) {
                            File tempf = new File(temp[i] + "/" + split[1]);
                            if (tempf.exists()) {
                                strImagePath = temp[i] + "/" + split[1];
                            }
                        }
                    }
                } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                    String id = DocumentsContract.getDocumentId(uri);
                    Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    Cursor cursor = null;
                    String column = "_data";
                    String[] projection = {
                            column
                    };

                    try {
                        cursor = getActivity().getContentResolver().query(contentUri, projection, null, null,
                                null);
                        if (cursor != null && cursor.moveToFirst()) {
                            final int column_index = cursor.getColumnIndexOrThrow(column);
                            strImagePath = cursor.getString(column_index);
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                } else if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                    String docId = DocumentsContract.getDocumentId(uri);
                    String[] split = docId.split(":");
                    String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    String selection = "_id=?";
                    String[] selectionArgs = new String[]{
                            split[1]
                    };

                    Cursor cursor = null;
                    String column = "_data";
                    String[] projection = {
                            column
                    };

                    try {
                        cursor = getActivity().getContentResolver().query(contentUri, projection, selection, selectionArgs,
                                null);
                        if (cursor != null && cursor.moveToFirst()) {
                            int column_index = cursor.getColumnIndexOrThrow(column);
                            strImagePath = cursor.getString(column_index);
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                } else if ("com.google.android.apps.docs.storage".equals(uri.getAuthority())) {
                    isImageFromGoogleDrive = true;
                }
            } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                Cursor cursor = null;
                String column = "_data";
                String[] projection = {
                        column
                };

                try {
                    cursor = getActivity().getContentResolver().query(uri, projection, null, null,
                            null);
                    if (cursor != null && cursor.moveToFirst()) {
                        int column_index = cursor.getColumnIndexOrThrow(column);
                        strImagePath = cursor.getString(column_index);
                    }
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                strImagePath = uri.getPath();
            }


            if (isImageFromGoogleDrive) {
                try {
                 /*   Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                    //  getDrawQR(bitmap);
                    Logwtf(" onActivity Result,Image Path", "ImagePath:=" + strImagePath + "\nBITE:" + bitmap.getByteCount() / 1024 + "\nRAwBITE:" + bitmap.getRowBytes() / 1024);
                  */
                    LoadPic("file://" + strImagePath);
                    //iv.setImageBitmap(bitmap);
                    //  tvImagePath.setText(getResources().getString(R.string.str_image_google_drive));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
               /* File f = new File(strImagePath);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
                //getDrawQR(bitmap);
                Logwtf(" onActivity Result,Image Path", "ImagePath:=" + strImagePath + "\nBITE:" + bitmap.getByteCount() / 1024 + "\nRAwBITE:" + bitmap.getRowBytes() / 1024);
               */
                LoadPic("file://" + strImagePath);
                //iv.setImageBitmap(bitmap);
                // tvImagePath.setText(getResources().getString(R.string.str_image_path) + ": " + strImagePath);
            }


        }
        // super.onActivityResult(requestCode, resultCode, data);
    }

   /* void getDrawQR(Bitmap bitmap) {

        LinearLayoutCompat.LayoutParams mLayoutParams = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        LinearLayout mLinearLayout = new LinearLayout(getContext());
        ImageView imageView = new ImageView(getContext());
        mLayoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mLinearLayout.addView(imageView, mLayoutParams);
        try {
            Dialog dialogQR = new Dialog(getContext());
            dialogQR.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogQR.setCancelable(true);
            WindowManager.LayoutParams dialogQRlp = dialogQR.getWindow().getAttributes();
            dialogQRlp.dimAmount = 1.00f;
            imageView.setImageBitmap(bitmap);
            dialogQR.addContentView(mLinearLayout, mLayoutParams);
            dialogQR.show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


        void loadImage(File f) {
        Picasso.with(getContext()).load(f).transform(new CircleTransformBorder()).resize(imgsize, imgsize).centerInside().into(frweAcivProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                Toast.makeText(getContext(), "Failed To Load Photo", Toast.LENGTH_SHORT).show();
                Picasso.with(getContext()).load(R.drawable.default_profile_large).transform(new CircleTransformBorder()).resize(imgsize, imgsize).centerInside().into(frweAcivProfile);
            }
        });

    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }*/

    void LoadPic(final String Path) {

        Picasso.with(getActivity().getApplicationContext()).load(Path).transform(new CircleTransformBorder()).resize(imageSoze, imageSoze).centerInside().into(frweAcivProfile);
        GetImage.subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                        Logwtf("onCompleted", "*-*-*Task Done*-*-");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logwtf("onError", "*-*-*ERRoR e*-*-" + e.toString());
                    }

                    @Override
                    public void onNext(String s) {
                        Logwtf("onNext", "file :" + s);

                        Bitmap bitmap = null;
                        try {
                            bitmap = Picasso.with(getActivity().getApplicationContext()).load("file://" + s).resize(imageSoze, imageSoze).centerInside().get();

                        } catch (IOException e) {
                            Logwtf("IOException", "Error :" + e);

                        } finally {
                            if (bitmap != null) {
                                //do whatever you wanna do with the picture.
                                //for me it was using my own cache
                                {
                                    // getDrawQR(bitmap);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                    encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                                    Logwtf("finally", "file in BASE64" + encodeImage.substring(0, 100));
                                    // uploadImage(encodeImage);

                                }
                            } else {
                                Logwtf("finally", "Bitmap Null");
                            }
                        }

                    }
                });

    }

    void DoRegister() {

        ShowDiaog("Registering ..Please Wait");
        String Name = frweAcetName.getText().toString().trim();
        String Surname = frweAcetSurname.getText().toString().trim();
        String Email = frweAcetEmail.getText().toString().trim();
        String Password = frweAcetPassword.getText().toString().trim();
        String PhoneNumber = frweAcetMobile.getText().toString().trim();
        String FB_Token = FirebaseInstanceId.getInstance().getToken();


        Logwtf("mRegisterUser", Name + " " + Surname + " " + Email + " " + Password + " " + PhoneNumber + " ");

      /*  Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/

        // SlimApi service = MyApplication.getService();
        Call<Login_Response> RegisterUserCall = service.RegisterUser(Name, Surname, Email, Password, PhoneNumber,
                encodeImage, mIsRegiFrom, mSelectedCity);
        RegisterUserCall.enqueue(new Callback<Login_Response>() {
            @Override
            public void onResponse(Response<Login_Response> response, Retrofit retrofit) {

                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (response != null) {

                        if (response.body().error.equals(false)) {

                            Toast.makeText(getContext(), "Registered  Successfully", Toast.LENGTH_SHORT).show();

                            MyDBHelper.putBoolean(MyConsta.KEY_IsLoggedIn, true);
                            MyDBHelper.putString(MyConsta.KEY_Email, response.body().email);
                            MyDBHelper.putString(MyConsta.KEY_UUID, response.body().apikey);
                            MyDBHelper.putString(MyConsta.KEY_Name, response.body().name);
                            MyDBHelper.putString(MyConsta.KEY_Surname, response.body().surname);
                            MyDBHelper.putBoolean(MyConsta.KEY_KeepMeLoggedIn, true);
                            MyDBHelper.putInt(MyConsta.KEY_REGISTEREDFROM, response.body().regifrom);
                            MyDBHelper.putInt(MyConsta.KEY_CID, response.body().mid);
                            MyDBHelper.putString(MyConsta.KEY_Phone, response.body().mobile);
                            MyDBHelper.putString(MyConsta.KEY_ProfileImage, response.body().profileimage);
                            MyDBHelper.putString(MyConsta.KEY_FullName, response.body().name + " " + response.body().surname);

                            //company info
                            MyDBHelper.putBoolean(MyConsta.KEY_IsCompanyAssined, response.body().isCompanyAssined);

                            if (response.body().isCompanyAssined) {

                                MyDBHelper.putString(MyConsta.KEY_CompanyID, String.valueOf(response.body().companyID));
                                MyDBHelper.putString(MyConsta.KEY_CompanyFName, response.body().companyFisrtName);
                                MyDBHelper.putString(MyConsta.KEY_CompanyLName, response.body().companyLastName);
                                MyDBHelper.putString(MyConsta.KEY_CompanyFullName, response.body().companyFisrtName + " " + response.body().companyLastName);

                            }

                            MyDBHelper.putBoolean(MyConsta.KEY_BIKE_ISAssigned, false);

                            MyDBHelper.putString(MyConsta.KEY_City, response.body().cityName);
                            MyDBHelper.putString(MyConsta.KEY_PostalCode, String.valueOf(response.body().cityZipCode));
                            MyDBHelper.commit();
                            // startActivity(new Intent(getContext(), Home_Activity.class));
                            //finish();
                            MyApplication.getInstance().UpdateNotifToken(response.body().mid, response.body().apikey, FB_Token);
                            Bundle b = new Bundle();
                            b.putString(MyConsta.KEY_Email, frweAcetEmail.getText().toString());
                            mListener.onRegistrationSuccess(b);
                            Toast.makeText(getContext(), "You are Registered Successfully", Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(getContext(), response.body().message, Toast.LENGTH_SHORT).show();
                        }
                    }

                } else if (response.code() == 201) {

                    Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message.toString());
                    Toast.makeText(getContext(), "Internal Error Try Again After Some Time", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Invalid Response", Toast.LENGTH_SHORT).show();
                }
                hideDialog();

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(), "Something went Wrong Please try after some time ", Toast.LENGTH_SHORT).show();
                Logwtf("onFailure", t.toString());
                hideDialog();
            }
        });

    }

    void ShowDiaog(String Msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog.setMessage(Msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null) {

            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    void getCities() {

        Call<Cities> mCitiesListCall = service.Get_Cities();
        mCitiesListCall.enqueue(new Callback<Cities>() {
            @Override
            public void onResponse(Response<Cities> response, Retrofit retrofit) {
                if (response.code() == MyConsta.RESPONSE_OK) {

                    if (response != null) {

                        if (response.body().error.equals(false)) {


                            frweAcspCompany.setAdapter(new MyCompanySpAdapter(response.body().citiesList, getContext()));
                            mCitiesList = response.body().citiesList;
                            Logwtf("Response Company", " Response" + response.raw());
                            //  Toast.makeText(getContext(), "Registered  Successfully", Toast.LENGTH_SHORT).show();

                        } else {


                        }
                    }

                } else if (response.code() == 201) {

                    Logwtf("Response Company", response.headers().toString() + "\n" + response.raw());

                    Toast.makeText(getContext(), "Invalid Response From Server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Invalid Response", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onRegistrationSuccess(Bundle b);
    }

}
