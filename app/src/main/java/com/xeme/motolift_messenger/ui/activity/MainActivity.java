package com.xeme.motolift_messenger.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.components.LoginService;
import com.xeme.motolift_messenger.components.MyLocationService;
import com.xeme.motolift_messenger.components.SocialMediaProvider;
import com.xeme.motolift_messenger.pojos.UserAccount;
import com.xeme.motolift_messenger.ui.fragment.FragConfirmEmail;
import com.xeme.motolift_messenger.ui.fragment.FragLoginRegisterMain;
import com.xeme.motolift_messenger.ui.fragment.FragRegisterWithEmail;
import com.xeme.motolift_messenger.ui.fragment.Frag_Login;
import com.xeme.motolift_messenger.utils.MyHelper;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

/*import com.rogalabs.lib.LoginView;
import com.rogalabs.lib.SocialUser;*/

public class MainActivity extends AppCompatActivity implements
        FragLoginRegisterMain.OnFragmentInteractionListener,
        FragRegisterWithEmail.OnFragmentInteractionListener,
        Frag_Login.OnFragmentInteractionListener,
        FragConfirmEmail.OnFragmentInteractionListener, GoogleApiClient.ConnectionCallbacks {
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    Toolbar toolbar;
    GoogleApiClient mGoogleApiClient;
    private SocialMediaProvider mProfile;

    private MyWebRequestReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        IntentFilter filter = new IntentFilter(MyWebRequestReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new MyWebRequestReceiver();
        registerReceiver(receiver, filter);

        //Check if connected

        InitViews();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (MyApplication.googleApiHelper.isConnected()) {

            mGoogleApiClient = MyApplication.googleApiHelper.getGoogleApiClient();
            MyApplication.googleApiHelper.connect();
            Logwtf("onCreate", " got Googleclient");
        } else {
            Logwtf("onCreate", " All ready connected");
            mGoogleApiClient = MyApplication.googleApiHelper.getGoogleApiClient();

        }

    }

    @Override
    public void onRegistrationSuccess(Bundle b) {

        FragConfirmEmail fragConfirmEmail=new FragConfirmEmail();
        fragConfirmEmail.setArguments(b);
        swapFrags(fragConfirmEmail);
        // mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }

    @Override
    public void onFragmentLoginSuccess() {
        Intent Serviceintent = new Intent(MainActivity.this, MyLocationService.class);
       startService(Serviceintent);

        GoToHome2();

    }

    @Override
    public void swapFrags(Fragment fragment) {

        final FragConfirmEmail fragConfirmEmail = new FragConfirmEmail();
        if (fragConfirmEmail == null) {
            Logwtf("in swapFrags", "Frag in null");
        }

        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout);

        if (fragment != null) {

            if (fragment == fragConfirmEmail) {

                transaction.replace(R.id.ma_fl_container, fragConfirmEmail);
                transaction.addToBackStack(null);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);


            } else {


                transaction.replace(R.id.ma_fl_container, fragment);
                transaction.addToBackStack(fragment.getTag());


            }

            transaction.commit();
        }
    }

    @Override
    public void LoginWith(int with) {


        switch (with) {

            case MyHelper.LOGIN_GOOGLEPLUS:

                mProfile = SocialMediaProvider.GOOGLE_PLUS;
                Intent intentGPlus = new Intent(this, LoginService.class);
                intentGPlus.putExtra(LoginService.EXTRA_PROFILE, mProfile);
                startService(intentGPlus);

                break;

            case MyHelper.LOGIN_FACEBOOK:

                mProfile = SocialMediaProvider.FACEBOOK;
                Intent intent = new Intent(this, LoginService.class);
                intent.putExtra(LoginService.EXTRA_PROFILE, mProfile);
                startService(intent);

                break;

            case MyHelper.LOGIN_LINKEDIN:

                mProfile = SocialMediaProvider.LINKED_IN;
                Intent intentLI = new Intent(this, LoginService.class);
                intentLI.putExtra(LoginService.EXTRA_PROFILE, mProfile);
                startService(intentLI);

                break;

        }
    }

    void InitViews() {

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.ma_fl_container, new FragLoginRegisterMain()).commit();
        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setTitle("Welcome To MotoLift");
                }
            }
        });

        toolbar.setNavigationOnClickListener(v -> {
            //onBackPressed();
        });

    }


    @Override
    public void onFragConfig() {

        GotoHome();
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Logwtf("onConnected", "Google Api Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {

            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(receiver);
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }


    private void GotoRegisterFrag(UserAccount mUserAccount,SocialMediaProvider Provider) {
        Fragment mFragment=FragRegisterWithEmail.newInstance(mUserAccount,Provider);
        mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout);
        mFragmentTransaction.replace(R.id.ma_fl_container, mFragment).commit();
    }


    void GotoHome(){

      /*  Intent i=new Intent(MainActivity.this,HomeActivity.class);
        startActivity(i);
        finish();*/


        Intent i = new Intent(MainActivity.this, MainActivity.class);
        overridePendingTransition(0, 0);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.finish();
        overridePendingTransition(0, 0);
        startActivity(i);

    }

    void GoToHome2(){
        Intent i = new Intent(MainActivity.this, HomeActivity.class);
        overridePendingTransition(0, 0);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        this.finish();
        overridePendingTransition(0, 0);
        startActivity(i);
    }



    public class MyWebRequestReceiver extends BroadcastReceiver {

        public static final String PROCESS_RESPONSE = "com.Motolift.intent.action.PROCESS_RESPONSE";

        @Override
        public void onReceive(Context context, Intent intent) {

            UserAccount mUserAccount=intent.getExtras().getParcelable("account");
            if (mUserAccount != null) {

                Logwtf("AccountName:","Name="+mUserAccount.getFullName()+" Provide:"+intent.getExtras().getSerializable("profile"));
                Toast.makeText(context, mUserAccount.getFullName(), Toast.LENGTH_SHORT).show();
             //  SocialMediaProvider provier=intent.getExtras().getSerializable("profile");
                GotoRegisterFrag(mUserAccount,(SocialMediaProvider) intent.getExtras().getSerializable("profile"));
            }
         else {
                Logwtf("Account Null","");
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
