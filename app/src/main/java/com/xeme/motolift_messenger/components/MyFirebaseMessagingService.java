package com.xeme.motolift_messenger.components;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.xeme.motolift_messenger.R;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIF_TYPE_SIMPLE = "0";
    public static final String NOTIF_TYPE_BIKE_ALOTED = "1";
    public static final String NOTIF_TYPE_BIKE_REMOVED = "2";

    MyDatahelper mDatahelper;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        mDatahelper = MyDatahelper.getInstance(this, MyConsta.My_Pref);

        Logwtf(MyConsta.TAG, "remoteMessage: " + remoteMessage);


        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
   /*     Logwtf(MyConsta.TAG, "Notif: " + remoteMessage.getNotification().toString());
        Logwtf(MyConsta.TAG, "RAW: " + remoteMessage.toString());*/
/*
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logwtf(MyConsta.TAG, "Message data payload: " + remoteMessage.getData());
            //sendNotification("Notification Received:"+remoteMessage.getFrom());
            sendNotification(remoteMessage);
        }*/

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logwtf(MyConsta.TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage);
        }


        if (remoteMessage.getData() != null) {
            if (remoteMessage.getData().containsKey("type")) {
                if (remoteMessage.getData().get("type").equals(NOTIF_TYPE_SIMPLE)) {

                    Logwtf("NotifType", "SimpleNotif");
                    sendNotification2(remoteMessage);

                }
                if (remoteMessage.getData().get("type").equals(NOTIF_TYPE_BIKE_ALOTED)) {


                    mDatahelper.putBoolean(MyConsta.KEY_BIKE_ISAssigned,true);
                    if (remoteMessage.getData().get("brand") != null) {
                        mDatahelper.putString(MyConsta.KEY_BIKE_BRAND, remoteMessage.getData().get("brand"));
                        Logwtf("NotifType", "Motobike Assign" + "Brand: " + (remoteMessage.getData().get("brand")));
                    }

                    if (remoteMessage.getData().get("bikename") != null) {
                        mDatahelper.putString(MyConsta.KEY_BIKE_NAME, remoteMessage.getData().get("bikename"));
                        Logwtf("NotifType", "Motobike Assign" + "BikeName: " + (remoteMessage.getData().get("bikename")));
                    }
                    if (remoteMessage.getData().get("model") != null) {
                        mDatahelper.putString(MyConsta.KEY_BIKE_MODEL, remoteMessage.getData().get("model"));
                        Logwtf("NotifType", "Motobike Assign" + "model: " + (remoteMessage.getData().get("model")));
                    }
                    if (remoteMessage.getData().get("patent") != null) {
                        mDatahelper.putString(MyConsta.KEY_BIKE_PATENT, remoteMessage.getData().get("patent"));
                        Logwtf("NotifType", "Motobike Assign" + "patent: " + (remoteMessage.getData().get("patent")));
                    }
                    if (remoteMessage.getData().get("id") != null) {
                        mDatahelper.putString(MyConsta.KEY_BIKE_ID, remoteMessage.getData().get("id"));
                        Logwtf("NotifType", "Motobike Assign" + "id: " + (remoteMessage.getData().get("id")));
                    }

                }
                if (remoteMessage.getData().get("type").equals(NOTIF_TYPE_BIKE_REMOVED)) {
                    mDatahelper.removeValue(MyConsta.KEY_BIKE_ID);
                    mDatahelper.removeValue(MyConsta.KEY_BIKE_BRAND);
                    mDatahelper.removeValue(MyConsta.KEY_BIKE_NAME);
                    mDatahelper.removeValue(MyConsta.KEY_BIKE_MODEL);
                    mDatahelper.removeValue(MyConsta.KEY_BIKE_PATENT);
                    mDatahelper.putBoolean(MyConsta.KEY_BIKE_ISAssigned,false);
                    Logwtf("NotifType", "Motobike REMOVED");
                }
            }
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     * <p>
     * //  * @param messageBody FCM message body received.
     */
    private void sendNotification(RemoteMessage message) {
        Intent intent = new Intent();
        intent.setAction(message.getNotification().getClickAction());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_marker_a)
                .setContentTitle(message.getNotification().getTitle())
                .setContentText(message.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



    private void sendNotification2(RemoteMessage message) {
        Intent intent=new Intent();
        intent.setAction(message.getData().get(MyConsta.NOTIF_CLICK_ACTION));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(message.getData().get(MyConsta.NOTIF_TITLE))
                .setContentText(message.getData().get(MyConsta.NOTIF_SUBTITLE))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());
    }
}