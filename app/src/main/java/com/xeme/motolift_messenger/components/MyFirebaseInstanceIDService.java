package com.xeme.motolift_messenger.components;

import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.xeme.motolift_messenger.MyApplication;
import com.xeme.motolift_messenger.pojos.Simple_Response;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.xeme.motolift_messenger.utils.MyHelper.Logwtf;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logwtf(MyConsta.TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {

        MyDatahelper MyDB = MyDatahelper.getInstance(this, MyConsta.My_Pref);
        String UUID = MyDB.getString(MyConsta.KEY_UUID, "");
        int UID = MyDB.getInt(MyConsta.KEY_CID, 0);

        if (UUID.length() > 0 && UID > 0) {


            Call<Simple_Response> UPDATE_FBT = MyApplication.getService().Update_FBT(UUID, UID, token);
            UPDATE_FBT.enqueue(new Callback<Simple_Response>() {
                @Override
                public void onResponse(Response<Simple_Response> response, Retrofit retrofit) {
                    if (response.code() == 200) {
                        if (response != null) {

                            if (response.body().error.equals(false)) {

                                Toast.makeText(MyFirebaseInstanceIDService.this, "Login Success", Toast.LENGTH_SHORT).show();


                            } else {

                                Toast.makeText(MyFirebaseInstanceIDService.this, response.body().message, Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else if (response.code() == 201) {

                        Logwtf("Response Register", response.headers().toString() + "\n" + response.raw() + "\n Response msg" + response.body().message.toString());

                        Toast.makeText(MyFirebaseInstanceIDService.this, "Invalid Response From Server", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyFirebaseInstanceIDService.this, "Invalid Response", Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Throwable t) {
                    Logwtf("onFailure", t.getCause().toString());
                }
            });
        }


    }
}