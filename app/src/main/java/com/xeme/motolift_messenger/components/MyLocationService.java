package com.xeme.motolift_messenger.components;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.xeme.motolift_messenger.utils.MyConsta;
import com.xeme.motolift_messenger.utils.MyDatahelper;

import java.util.Calendar;

public class MyLocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    // Gpslocation gpslocation=new Gpslocation(MyLocationService.this);


    static Context mContext;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Location mCurrentLocation;
    boolean isconnected = false;
    Calendar mCalendar;
    int mid;
    DatabaseReference databaseReference;
    MyDatahelper mypref;

    public MyLocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = MyLocationService.this;
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        createLocationRequest();
        Log.wtf(MyConsta.TAG, "********Error******* : Service Started" + mid);

        mypref = MyDatahelper.getInstance(mContext, "Transportation");
        mid = mypref.getInt(MyConsta.KEY_CID, 0);
        System.out.println("PREF" + mid);

        if (mid <= 0) {
            Log.wtf(MyConsta.TAG, "********Error******* : killing service" + mid);
            this.stopSelf();
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();

        mCalendar = Calendar.getInstance();
        return Service.START_STICKY;
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;

        databaseReference.child("Messenger").child("location").child(String.valueOf(mid)).setValue(location);

        Log.wtf(MyConsta.TAG, "********Location Updated******* :" + location);
        //MyLocation myLocation=new MyLocation(getLat(),getLng(),getTime());
        // myLocation.save();
        // MyConsta.LogIt("onLocationChanged of Services",location.toString());
    }

    private void startLocationUpdates() {


        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        } catch (SecurityException s) {

            Log.wtf(MyConsta.TAG, "********Error******* :" + s);
        } catch (Exception e) {
            Log.wtf(MyConsta.TAG, "********Error******* :" + e);
        }

    }

   /* private void stopLocationUpdates() {
        try {

            Gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    Gpslocation.mGoogleApiClient);

            if (Gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        Gpslocation.mGoogleApiClient, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }*/

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {
            isconnected = true;
            startLocationUpdates();

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                // Determine whether a Geocoder is available.
                if (!Geocoder.isPresent()) {

                    return;
                }


            }
        } catch (SecurityException s) {

            Log.wtf(MyConsta.TAG, "********Error******* :" + s);
        } catch (Exception x) {
            Log.wtf(MyConsta.TAG, "********Error******* :" + x);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        isconnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(MyConsta.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(MyConsta.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    long getTime() {

        if (mCalendar != null) {

            return mCalendar.getTimeInMillis();
        } else {
            mCalendar = Calendar.getInstance();
            return mCalendar.getTimeInMillis();

        }

    }


    public double getLat() {

        return mCurrentLocation.getLatitude();
    }

    public double getLng() {

        return mCurrentLocation.getLongitude();
    }
}
