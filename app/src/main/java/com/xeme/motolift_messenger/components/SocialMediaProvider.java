package com.xeme.motolift_messenger.components;

public enum SocialMediaProvider {
    FACEBOOK,
    TWITTER,
    GOOGLE_PLUS,
    INSTAGRAM,
    LINKED_IN,
    YAHOO,
    WINDOWS_LIVE,
    GITHUB
}